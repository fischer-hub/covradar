"""
    Test for extracting data from gisaid JSON format
"""
import unittest
from backend.scripts.extract_gisaid_json import create_files
from backend.tests.conftest import dpath
import os
import tempfile


class TestGisaidExtractoin(unittest.TestCase):
    json_path = dpath("testdata/gisaid_extraction/test_gisaid_100.json")
    tsv_result_path = dpath("testdata/gisaid_extraction/result.tsv")
    fasta_result_path = dpath("testdata/gisaid_extraction/result.fasta")
    temp_fasta = tempfile.mkstemp()[1]
    temp_tsv = tempfile.mkstemp()[1]
    skip_germany = False

    def test_gisaid_extraction(self):
        with open(self.tsv_result_path) as tsv_expected:
            with open(self.fasta_result_path) as fasta_expected:
                create_files(
                    self.json_path,
                    tsv_path=self.temp_tsv,
                    fasta_path=self.temp_fasta,
                    skip_germany=self.skip_germany,
                )
                with open(self.temp_tsv) as tsv_actual:
                    with open(self.temp_fasta) as fasta_actual:
                        assert [row for row in tsv_expected] == [
                            row for row in tsv_actual
                        ]
                        assert [row for row in fasta_expected] == [
                            row for row in fasta_actual
                        ]

    def tearDown(self) -> None:
        self.delete_temp_test_files()
        return super().tearDown()

    def delete_temp_test_files(self):
        if os.path.exists(self.temp_fasta):
            os.remove(self.temp_fasta)
        if os.path.exists(self.temp_tsv):
            os.remove(self.temp_tsv)
