import unittest
from io import StringIO

import numpy as np
import pandas as pd
from numpy.testing import assert_array_equal
from pandas.testing import assert_frame_equal

from backend.scripts.get_or_delete_region import (
    parse_args,
    read_metadata,
    match,
    get_mask,
    filter_metadata,
    get_strains,
    write_metadata,
    write_sequence,
    filter_genomes,
)
from backend.tests.conftest import dpath


class TestSplitByRegion(unittest.TestCase):
    def test_parser_1(self):
        parser = parse_args(
            [
                "--genomes",
                "genomes.fasta",
                "--metadata",
                "metadata.tsv",
                "--output-genomes",
                "output.fasta",
                "--output-metadata",
                "output.tsv",
                "--region",
                "Germany",
            ]
        )
        self.assertTrue(parser.genomes)
        self.assertEqual(parser.genomes, "genomes.fasta")
        self.assertTrue(parser.metadata)
        self.assertEqual(parser.metadata, "metadata.tsv")
        self.assertTrue(parser.output_genomes)
        self.assertEqual(parser.output_genomes, "output.fasta")
        self.assertTrue(parser.output_metadata)
        self.assertEqual(parser.output_metadata, "output.tsv")
        self.assertTrue(parser.region)
        self.assertEqual(parser.region, "Germany")
        self.assertFalse(parser.delete)
        self.assertFalse(parser.progress)

    def test_parser_2(self):
        parser = parse_args(
            [
                "--genomes",
                "genomes.fasta",
                "--metadata",
                "metadata.tsv",
                "--output-genomes",
                "output.fasta",
                "--output-metadata",
                "output.tsv",
                "--region",
                "Brazil",
                "--delete",
                "--progress",
            ]
        )
        self.assertTrue(parser.region)
        self.assertEqual(parser.region, "Brazil")
        self.assertTrue(parser.delete)
        self.assertTrue(parser.progress)

    def test_read_metadata(self):
        input = StringIO()
        input.write("strain\tcountry\n")
        input.write("1\tGermany\n")
        input.write("2\tBrazil\n")
        input.seek(0)
        ground_truth = pd.DataFrame(
            {"strain": [1, 2], "country": ["Germany", "Brazil"]}
        )
        progress = False
        metadata = read_metadata(input, progress)
        assert_frame_equal(ground_truth, metadata)

    def test_match(self):
        self.assertTrue(match("Germany", "Germany"))
        self.assertTrue(match("germany", "Germany"))
        self.assertTrue(match("Germany", "germany"))
        self.assertTrue(match("Brazil", "Brazil"))
        self.assertTrue(match("brazil", "Brazil"))
        self.assertTrue(match("Brazil", "brazil"))
        self.assertFalse(match("Brazil", "Germany"))
        self.assertFalse(match(np.nan, "Germany"))
        self.assertFalse(match("Germany", np.nan))

    def test_get_mask(self):
        df = pd.DataFrame(
            {"strain": [1, 2, 3], "country": ["Germany", "Brazil", "Germany"]}
        )
        delete = False
        progress = False
        assert_array_equal(
            np.array([True, False, True]), get_mask(df, "Germany", delete, progress)
        )
        assert_array_equal(
            np.array([False, True, False]), get_mask(df, "Brazil", delete, progress)
        )
        assert_array_equal(
            np.array([True, True, True]), get_mask(df, "Global", delete, progress)
        )
        assert_array_equal(
            np.array([False, False, False]), get_mask(df, "Australia", delete, progress)
        )
        delete = True
        assert_array_equal(
            np.array([True, False, True]), get_mask(df, "Brazil", delete, progress)
        )

    def test_filter_metadata(self):
        df = pd.DataFrame(
            {"strain": [1, 2, 3], "country": ["Germany", "Brazil", "Germany"]}
        )
        assert_frame_equal(
            df[df["country"] == "Germany"], filter_metadata(df, [True, False, True])
        )
        assert_frame_equal(
            df[df["country"] == "Brazil"], filter_metadata(df, [False, True, False])
        )
        assert_frame_equal(df, filter_metadata(df, [True, True, True]))
        assert_frame_equal(
            df[df["country"] == "Australia"], filter_metadata(df, [False, False, False])
        )

    def test_get_strains(self):
        df = pd.DataFrame(
            {"strain": [1, 2, 3], "country": ["Germany", "Brazil", "Germany"]}
        )
        ground_truth = {"1", "2", "3"}
        strains = get_strains(df)
        self.assertSetEqual(ground_truth, strains)

    def test_write_metadata(self):
        df = pd.DataFrame({"strain": [1, 2], "country": ["Germany", "Brazil"]})
        output = StringIO()
        write_metadata(df, output)
        output.seek(0)
        ground_truth = "strain\tcountry\n1\tGermany\n2\tBrazil\n"
        self.assertEqual(ground_truth, output.read())

    def test_write_sequence_1(self):
        ground_truth = ">Test_1\nATCG\n"
        output = StringIO()
        id = "Test_1"
        sequence = "ATCG"
        write_sequence(output, id, sequence)
        output.seek(0)
        content = output.read()
        self.assertEqual(content, ground_truth)

    def test_write_sequence_2(self):
        ground_truth = ">Test_1\nAT\nCG\n>Test_2\nAT\n"
        output = StringIO()
        write_sequence(output, "Test_1", "ATCG", 2)
        write_sequence(output, "Test_2", "AT", 2)
        output.seek(0)
        content = output.read()
        self.assertEqual(content, ground_truth)

    def test_write_sequence_3(self):
        ground_truth = ">Test_1\nAT\nC\n>Test_2\nA\n"
        output = StringIO()
        write_sequence(output, "Test_1", "ATC", 2)
        write_sequence(output, "Test_2", "A", 2)
        output.seek(0)
        content = output.read()
        self.assertEqual(content, ground_truth)

    def test_filter_genomes_1(self):
        ground_truth = ">1\nATCG\n>2\nCGAT\n"
        genomes = dpath("testdata/get_or_delete_region/genomes.fasta")
        output = StringIO()
        strains = {"1", "2"}
        progress = False
        filter_genomes(genomes, output, strains, progress)
        output.seek(0)
        content = output.read()
        self.assertEqual(ground_truth, content)

    def test_filter_genomes_2(self):
        ground_truth = ">1\nATCG\n"
        genomes = dpath("testdata/get_or_delete_region/genomes.fasta")
        output = StringIO()
        strains = {"1"}
        progress = False
        filter_genomes(genomes, output, strains, progress)
        output.seek(0)
        content = output.read()
        self.assertEqual(ground_truth, content)

    def test_filter_genomes_3(self):
        ground_truth = ">2\nCGAT\n"
        genomes = dpath("testdata/get_or_delete_region/genomes.fasta")
        output = StringIO()
        strains = {"2"}
        progress = False
        filter_genomes(genomes, output, strains, progress)
        output.seek(0)
        content = output.read()
        self.assertEqual(ground_truth, content)


if __name__ == "__main__":
    unittest.main()
