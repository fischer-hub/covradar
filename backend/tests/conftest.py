import os


def dpath(path):
    """
    get path to a data file (relative to the directory this
    test lives in)
    """
    return os.path.realpath(os.path.join(os.path.dirname(__file__), path))


def delete(files: list) -> None:
    """
    Delete a list of files if they exist.

    Parameters
    ----------
    files : list
        Files to delete.
    """
    for file in files:
        if os.path.exists(file):
            os.remove(file)