"""
Test cases for rule voc_modify
"""
import unittest
import numpy as np
import pandas as pd
from backend.scripts.variant_calling import vcf_table_header, vcf_row_generation
from backend.tests.conftest import dpath
import ray

class Test1(unittest.TestCase):
    """Test suite VCF generation"""
    def setUp(self):
        self.msa = dpath("testdata/variant_calling/test1_msa_ref.fasta") #msa
        self.ref = dpath("testdata/variant_calling/test1_ref.fasta") #ref
        self.correct = dpath("testdata/variant_calling/test1_result.tsv") #correct
        ray.init(local_mode=True)

    def test_1(self):
        """Tests with dummy data covering possible mutation notations.
        """
        first, bases_by_position, vcf_header = vcf_table_header(self.msa, self.ref)
        
        row_part_ids = []
        for el in bases_by_position:
            row_part_ids.append(vcf_row_generation.remote(first, el))

        rows = []
        for rid in row_part_ids:
            rows_part = ray.get(rid)
            if rows_part is not None:
                rows.append(rows_part)

        header = vcf_header.split("\n")[-1].split("\t")
        result = pd.DataFrame(rows, columns=header)
        result = result.apply(pd.to_numeric, errors="ignore")
        correct = pd.read_csv(self.correct, sep="\t")
        pd.testing.assert_frame_equal(result, correct, check_dtype=False, check_index_type=False)
        
    def tearDown(self):
        ray.shutdown()

class Test2(unittest.TestCase):
    """Test suite VCF generation"""
    def setUp(self):
        self.msa = dpath("testdata/variant_calling/test2_msa_ref.fasta") #msa
        self.ref = dpath("testdata/variant_calling/test2_ref.fasta") #ref
        self.correct = dpath("testdata/variant_calling/test2_result.tsv") #correct
        ray.init(local_mode=True)

    def test_2(self):
        """Tests with dummy data covering possible mutation notations.
        """
        first, bases_by_position, vcf_header = vcf_table_header(self.msa, self.ref)
        # creates vcf rows
        row_part_ids = []
        for el in bases_by_position:
            row_part_ids.append(vcf_row_generation.remote(first, el))

        rows = []
        for rid in row_part_ids:
            rows_part = ray.get(rid)
            if rows_part is not None:
                rows.append(rows_part)

        header = vcf_header.split("\n")[-1].split("\t")
        result = pd.DataFrame(rows, columns=header)
        result = result.apply(pd.to_numeric, errors="ignore")
        correct = pd.read_csv(self.correct, sep="\t")
        pd.testing.assert_frame_equal(result, correct, check_dtype=False, check_index_type=False)
        
    def tearDown(self):
        ray.shutdown()


class Test3(unittest.TestCase):
    """Test suite VCF generation"""
    def setUp(self):
        self.msa = dpath("testdata/variant_calling/test3_msa_ref.fasta") #msa
        self.ref = dpath("testdata/variant_calling/test3_ref.fasta") #ref
        self.correct = dpath("testdata/variant_calling/test3_result.tsv") #correct
        ray.init(local_mode=True)

    def test_3(self):
        """Tests with dummy data covering possible mutation notations.
        """
        first, bases_by_position, vcf_header = vcf_table_header(self.msa, self.ref)
        # creates vcf rows
        row_part_ids = []
        for el in bases_by_position:
            row_part_ids.append(vcf_row_generation.remote(first, el))

        rows = []
        for rid in row_part_ids:
            rows_part = ray.get(rid)
            if rows_part is not None:
                rows.append(rows_part)
        
        header = vcf_header.split("\n")[-1].split("\t")
        result = pd.DataFrame(rows, columns=header)
        result = result.apply(pd.to_numeric, errors="ignore")
        correct = pd.read_csv(self.correct, sep="\t")
        pd.testing.assert_frame_equal(result, correct, check_dtype=False, check_index_type=False)
        
    def tearDown(self):
        ray.shutdown()


class Test4(unittest.TestCase):
    """Test suite VCF generation"""
    def setUp(self):
        self.msa = dpath("testdata/variant_calling/test4_msa_ref.fasta") #msa
        self.ref = dpath("testdata/variant_calling/test4_ref.fasta") #ref
        self.correct = dpath("testdata/variant_calling/test4_result.tsv") #correct
        ray.init(local_mode=True)

    def test_4(self):
        """Tests with dummy data covering possible mutation notations.
        """
        first, bases_by_position, vcf_header = vcf_table_header(self.msa, self.ref)
        
        # creates vcf rows
        row_part_ids = []
        for el in bases_by_position:
            row_part_ids.append(vcf_row_generation.remote(first, el))

        rows = []
        for rid in row_part_ids:
            rows_part = ray.get(rid)
            if rows_part is not None:
                rows.append(rows_part)
        
        header = vcf_header.split("\n")[-1].split("\t")
        result = pd.DataFrame(rows, columns=header)
        result = result.apply(pd.to_numeric, errors="ignore")
        correct = pd.read_csv(self.correct, sep="\t")
        pd.testing.assert_frame_equal(result, correct, check_dtype=False, check_index_type=False)
        
    def tearDown(self):
        ray.shutdown()


class Test5(unittest.TestCase):
    """Test suite VCF generation"""
    def setUp(self):
        self.msa = dpath("testdata/variant_calling/test5_global-msa-25-08-2020_and_reference.fasta") #msa
        self.ref = dpath("testdata/variant_calling/test5_ref.fasta") #ref
        self.correct = dpath("testdata/variant_calling/test5_result.tsv") #correct
        ray.init(local_mode=True)

    def test_5(self):
        """Tests with dummy data covering possible mutation notations.
        """
        first, bases_by_position, vcf_header = vcf_table_header(self.msa, self.ref)
        
        # creates vcf rows
        row_part_ids = []
        for el in bases_by_position:
            row_part_ids.append(vcf_row_generation.remote(first, el))

        rows = []
        for rid in row_part_ids:
            rows_part = ray.get(rid)
            if rows_part is not None:
                rows.append(rows_part)
        
        header = vcf_header.split("\n")[-1].split("\t")
        result = pd.DataFrame(rows, columns=header)
        result = result.apply(pd.to_numeric, errors="ignore")
        result = result.sort_values(by=['POS'])
        correct = pd.read_csv(self.correct, sep="\t") # delete Unamed
        pd.testing.assert_frame_equal(result, correct, check_dtype=False)
        
    def tearDown(self):
        ray.shutdown()


class Test6(unittest.TestCase):
    """Test suite VCF generation"""
    def setUp(self):
        self.msa = dpath("testdata/variant_calling/test6_msa_ref.fasta") #msa
        self.ref = dpath("testdata/variant_calling/test6_ref.fasta") #ref
        self.correct = dpath("testdata/variant_calling/test6_result.tsv") #correct
        ray.init(local_mode=True)

    def test_6(self):
        """Tests with dummy data covering possible mutation notations.
        """
        first, bases_by_position, vcf_header = vcf_table_header(self.msa, self.ref)
        
        # creates vcf rows
        row_part_ids = []
        for el in bases_by_position:
            row_part_ids.append(vcf_row_generation.remote(first, el))

        rows = []
        for rid in row_part_ids:
            rows_part = ray.get(rid)
            if rows_part is not None:
                rows.append(rows_part)
        
        header = vcf_header.split("\n")[-1].split("\t")
        result = pd.DataFrame(rows, columns=header)
        result = result.apply(pd.to_numeric, errors="ignore")
        correct = pd.read_csv(self.correct, sep="\t")
        pd.testing.assert_frame_equal(result, correct, check_dtype=False, check_index_type=False)
        
    def tearDown(self):
        ray.shutdown()


if __name__ == '__main__':
    unittest.main()
