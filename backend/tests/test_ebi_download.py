"""
    Test for extracting data from gisaid JSON format
"""
import unittest
from backend.scripts.get_ebi_data import get_ebi_data
from backend.tests.conftest import dpath
import os
import tempfile
import pandas as pd
from Bio import SeqIO
import gzip

class TestEbiDownload(unittest.TestCase):
    tsv_result_path = dpath('testdata/ebi_download/result.tsv.gz')
    fasta_result_path = dpath('testdata/ebi_download/result.fasta.gz')

    # files containing nonexistent sequences. Used to test out-of-sync and wrong cache
    modified_tsv_cache = dpath('testdata/ebi_download/modified.tsv.gz')
    modified_fasta_cache = dpath('testdata/ebi_download/modified.fasta.gz')

    def setUp(self) -> None:
        self.temp_fasta = tempfile.mkstemp()[1]
        self.temp_tsv = tempfile.mkstemp()[1]
        return super().setUp()

    def tearDown(self) -> None:
        os.remove(self.temp_fasta)
        os.remove(self.temp_tsv)
        return super().tearDown()

    def test_ebi_download(self):
        get_ebi_data(self.temp_fasta, self.temp_tsv, seqs_per_worker=10, limit=100,compress=True)
        self.compare_files(self.temp_fasta, self.temp_tsv)

    def test_ebi_caching(self):
        get_ebi_data(self.temp_fasta, self.temp_tsv, seqs_per_worker=10, limit=50, compress=True)
        get_ebi_data(self.temp_fasta, self.temp_tsv, seqs_per_worker=10, limit=100,
                        old_fasta_path=self.temp_fasta, old_tsv_path=self.temp_tsv,compress=True)
        self.compare_files(self.temp_fasta, self.temp_tsv)

    # test handling of cache with Fasta and Tsv files that contain different sequences
    def test_invalid_cache(self):
        get_ebi_data(self.temp_fasta, self.temp_tsv, seqs_per_worker=10, limit=100, compress=True,
                        old_fasta_path=self.fasta_result_path, old_tsv_path=self.modified_tsv_cache)
        self.compare_files(self.temp_fasta, self.temp_tsv)

    # test handling of cache with nonexistent sequences
    def test_outdated_cache(self):
        get_ebi_data(self.temp_fasta, self.temp_tsv, seqs_per_worker=10, limit=100, compress=True,
                        old_fasta_path=self.modified_fasta_cache, old_tsv_path=self.modified_tsv_cache)
        self.compare_files(self.temp_fasta, self.temp_tsv)


    def compare_files(self, candidate_fasta, candidate_tsv):
        # Check TSV
        with gzip.open(candidate_tsv, mode='rt') as candidate_handle:
            with gzip.open(self.tsv_result_path, mode='rt') as correct_handle:
                correct = pd.read_csv(correct_handle, sep='\t', index_col='strain')
                candidate = pd.read_csv(candidate_handle, sep='\t', index_col='strain')
                correct.sort_index(inplace=True)
                candidate.sort_index(inplace=True)
                assert correct.equals(candidate)

        # Check FASTA
        with gzip.open(candidate_fasta, mode='rt') as candidate_handle:
            with gzip.open(self.fasta_result_path, mode='rt') as correct_handle:
                correct = SeqIO.parse(correct_handle, 'fasta')
                candidate = SeqIO.parse(candidate_handle, 'fasta')
                oldList = []
                newList = []
                for r in correct:
                    oldList.append(r)
                for r in candidate:
                    newList.append(r)

                oldList.sort(key=lambda a: a.id)
                newList.sort(key=lambda a: a.id)

                for [a, b] in zip(oldList, newList):
                    assert a.id == b.id
                    assert a.seq == b.seq
                    assert a.name == b.name
                    assert a.seq == b.seq
