import unittest
from io import StringIO

import numpy as np
from numpy.testing import assert_array_equal

from backend.scripts.consensus import (
    parse_args,
    load_sequences,
    split,
    write_sequence,
    compute_consensus,
    get_identifier,
)
from backend.tests.conftest import dpath


class TestConsensus(unittest.TestCase):
    def test_parser(self):
        parser = parse_args(
            ["--input", "input.fasta", "--output", "output.fasta", "--region", "Brazil"]
        )
        self.assertTrue(parser.input)
        self.assertEqual(parser.input, "input.fasta")
        self.assertTrue(parser.output)
        self.assertEqual(parser.output, "output.fasta")
        self.assertTrue(parser.region)
        self.assertEqual(parser.region, "Brazil")

    def test_split_1(self):
        ground_truth = np.array(["A", "T", "C", "G"])
        result = split("ATCG")
        assert_array_equal(ground_truth, result)

    def test_split_2(self):
        ground_truth = np.array(["T", "A", "G", "C"])
        result = split("TAGC")
        assert_array_equal(ground_truth, result)

    def test_load_sequences(self):
        genomes = dpath("testdata/consensus/genomes.fasta")
        sequences = load_sequences(genomes)
        ground_truth = np.array([["A", "T", "C", "G"], ["A", "A", "A", "A"]])
        assert_array_equal(ground_truth, sequences)

    def test_write_sequence_1(self):
        ground_truth = ">Test_1\nATCG\n"
        output = StringIO()
        id = "Test_1"
        sequence = "ATCG"
        write_sequence(output, id, sequence)
        output.seek(0)
        content = output.read()
        self.assertEqual(content, ground_truth)

    def test_write_sequence_2(self):
        ground_truth = ">Test_1\nAT\nCG\n>Test_2\nAT\n"
        output = StringIO()
        write_sequence(output, "Test_1", "ATCG", 2)
        write_sequence(output, "Test_2", "AT", 2)
        output.seek(0)
        content = output.read()
        self.assertEqual(content, ground_truth)

    def test_write_sequence_3(self):
        ground_truth = ">Test_1\nAT\nC\n>Test_2\nA\n"
        output = StringIO()
        write_sequence(output, "Test_1", "ATC", 2)
        write_sequence(output, "Test_2", "A", 2)
        output.seek(0)
        content = output.read()
        self.assertEqual(content, ground_truth)

    def test_compute_consensus(self):
        sequences = np.array(
            [
                ["A", "A", "T", "C", "G", "A", "X"],
                ["T", "A", "A", "C", "G", "-", "X"],
                ["C", "T", "T", "G", "C", "-", "X"],
            ]
        )
        consensus = compute_consensus(sequences)
        ground_truth = "AATCG-N"
        self.assertEqual(ground_truth, consensus)

    def test_get_identifier(self):
        input_file = "results/website/split_by_date/Japan/2020W24.fasta"
        region = "Japan"
        identifier = get_identifier(region, input_file)
        ground_truth = "Japan_2020W24"
        self.assertEqual(ground_truth, identifier)
