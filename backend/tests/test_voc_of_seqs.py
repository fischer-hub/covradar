"""
Test cases for rule voc_of_seqs_table
"""
import unittest
import pandas as pd
from backend.scripts.voc_of_seqs import _voc_lookup_creator, ini, create_lookup, refine_lookup, create_voc_of_seq, batch
from backend.tests.conftest import dpath
import ray


class TestVocLookupCreator(unittest.TestCase):
    """Test suite helper function _voc_lookup_creator()"""
    def setUp(self):
        self.test1 = ["C21614T", "test1", [200]]
        self.test2 = ["del:21767:6", "test2", [100, 101, 102, 103, 104, 105]]
        self.test3 = ["ins:21767:6:ATCATG", "test3", [100]]
        self.test4 = ["G22132C/G22132T", "test4", [10]]
        self.test5 = ["G23012A,A23063T", "test5", [10, 100]]
        self.test6 = ["G23012C+A23013C", "test6", [10, 11]]
        self.test7 = ["G23012C+A23013C/A23013T", "test7", [10, 100]]
        self.test8 = ["G23012C+del:21767:6", "test8", [10, 100, 101, 102, 103, 104, 105]]
        self.test9 = ["ins:21767:6:ATCATG,C21614T", "test9", [100, 200]]
        self.test10 = ["del:21767:6+G22132C/G22132T,ins:21767:6:ATCATG,C21614T", "test10",
                       [100, 101, 102, 103, 104, 105, 200, 250, 300]]

    def test_vocs(self):
        """
            Tests possible mutation notations. The mutation can be a SNP, deletion or insertion,
            e.g.:
            - C21614T
            - del:21767:6
            - ins:21767:6:ATCATG
            Mutations compositions are also possible:
            - G22132C/G22132T
            - G23012A,A23063T
            - G23012C+A23013C
            The separators "," and "+" can combine multiple formats.
            - G23012C+A23013C/A23013T
            - G23012C+del:21767:6
            - ins:21767:6:ATCATG,C21614T
        """
        result1 = {'mut': ['T'], 'aa_name': 'test1', 'com': {}}
        result2 = {'mut': ['-'], 'aa_name': 'test2', 'com': {101:["-"], 102:["-"], 103:["-"],
                                                             104:["-"], 105:["-"]}}
        result3 = {'mut': ['A'], 'aa_name': 'test3', 'com': {101:["T"], 102:["C"], 103:["A"],
                                                             104:["T"], 105:["G"]}}
        result4 = {'mut': ['C', 'T'], 'aa_name': 'test4', 'com': {}}
        result5 = {'mut': ['A'], 'aa_name': 'test5', 'com': {100: ['T']}}
        result6 = {'mut': ['C'], 'aa_name': 'test6', 'com': {11: ['C']}}
        result7 = {'mut': ['C'], 'aa_name': 'test7', 'com': {100: ['C', 'T']}}
        result8 = {'mut': ['C'], 'aa_name': 'test8', 'com': {100:['-'], 101:["-"], 102:["-"],
                                                             103:["-"], 104:["-"], 105:["-"]}}
        result9 = {'mut': ['A'], 'aa_name': 'test9', 'com': {101:["T"], 102:["C"], 103:["A"],
                                                             104:["T"], 105:["G"], 200:["T"]}}
        result10 = {'mut': ['-'], 'aa_name': 'test10', 'com': {101:["-"], 102:["-"], 103:["-"],
                                                               104:["-"], 105:["-"],
                                                               200:["C", "T"], 250:["A"],
                                                               251:["T"], 252:["C"], 253:["A"],
                                                               254:["T"], 255:["G"], 300:["T"]}}

        self.assertEqual(result1, _voc_lookup_creator(*self.test1))
        self.assertEqual(result2, _voc_lookup_creator(*self.test2))
        self.assertEqual(result3, _voc_lookup_creator(*self.test3))
        self.assertEqual(result4, _voc_lookup_creator(*self.test4))
        self.assertEqual(result5, _voc_lookup_creator(*self.test5))
        self.assertEqual(result6, _voc_lookup_creator(*self.test6))
        self.assertEqual(result7, _voc_lookup_creator(*self.test7))
        self.assertEqual(result8, _voc_lookup_creator(*self.test8))
        self.assertEqual(result9, _voc_lookup_creator(*self.test9))
        self.assertEqual(result10, _voc_lookup_creator(*self.test10))

class TestVocOfSeqs(unittest.TestCase):
    def setUp(self):
        self.snp_path = dpath("testdata/voc_of_seqs/vcf.vcf.gz")
        self.voc_path = dpath("testdata/voc_of_seqs/voc.tsv")
        self.correct_path = dpath("testdata/voc_of_seqs/result.tsv")
        self.batch_size = 1
        ray.init(local_mode=True)

    def test_voc_of_seqs(self):

        vcf_data, nt_col, aa_col, msa_positions = ini(self.snp_path, self.voc_path)


        li = list(zip(nt_col, aa_col, msa_positions))
        voc_lookup_list_ids = []
        for x in batch(li, self.batch_size):
            voc_lookup_list_ids.append(create_lookup.remote(x))
        voc_lookup_list = []
        for el in voc_lookup_list_ids:
            voc_lookup_list.extend(ray.get(el))

        voc_lookup_bool_ids = []
        for v in batch(voc_lookup_list, self.batch_size):
            voc_lookup_bool_ids.append(refine_lookup.remote(vcf_data, v))
        voc_lookup_bool = []
        for el in voc_lookup_bool_ids:
            voc_lookup_bool.extend(ray.get(el))

        voc_seq_table_list_ids = []
        for b in batch(voc_lookup_bool, self.batch_size):
            voc_seq_table_list_ids.append(create_voc_of_seq.remote(vcf_data, self.snp_path, b))
        voc_seq_table_list = []
        for el in voc_seq_table_list_ids:
            voc_seq_table_list.extend(ray.get(el))

        voc_seq_table = pd.DataFrame(voc_seq_table_list, columns=["sequence_name", "variant"])
        voc_seq_table["bool"] = True
        voc_seq_table = voc_seq_table.set_index(["sequence_name", "variant"]).unstack().fillna(False)
        voc_seq_table.columns = voc_seq_table.columns.droplevel()
        voc_seq_table.columns.name = ""
        voc_seq_table = voc_seq_table.sort_index()
        voc_seq_table = voc_seq_table.sort_index(axis=1)

        correct_table = pd.read_csv(self.correct_path, sep="\t", index_col="sequence_name")
        s = correct_table.unstack()
        df = s.mask(s==False).dropna().unstack().fillna(False)
        df = df.T
        df = df.sort_index()
        df = df.sort_index(axis=1)
        df.columns.name = ""

        pd.testing.assert_frame_equal(df, voc_seq_table, check_dtype=False)
        
    def tearDown(self):
        ray.shutdown()

if __name__ == '__main__':
    unittest.main()
