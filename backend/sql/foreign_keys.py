import mysql.connector
from tqdm import tqdm
foreign_keys = [
    # (origin_table_name, linked_table_name, origin_column_name, linked_table_name)
        ('global_metadata','global_SNP','strain_id','strain_id'),
        ('global_metadata','sequence_voc','strain_id','strain_id'),
        ('variants_of_concern','sequence_voc','id','voc_id'),
        ('consensus', 'base_count','region, date', 'region, date' )
]

# Drops foreign key. Linked_table is where the foreign key is defined
# This requires following a fk naming convention of 
# fk_<origin_table>_<linked_table> 
def drop_fk(conn, origin_table, linked_table):
    cursor = conn.cursor()
    # https://stackoverflow.com/questions/17161496/drop-foreign-key-only-if-it-exists
    try:
        cursor.execute(f"""
        ALTER TABLE {linked_table} DROP FOREIGN KEY fk_{origin_table}_{linked_table}
        """)
    except mysql.connector.Error as e:
        if e.errno == mysql.connector.errorcode.ER_CANT_DROP_FIELD_OR_KEY:
            # fk doesn't exist
            pass
        elif e.errno == mysql.connector.errorcode.ER_NO_SUCH_TABLE:
            # table doesn't exist
            pass
        else:
            raise
    finally:
        cursor.close()

def add_fk(conn, origin_table, linked_table, origin_column, linked_column):
    cursor = conn.cursor()
    # cursor.execute(f"""
    #     ALTER TABLE {linked_table} ADD CONSTRAINT `fk_{origin_table}_{linked_table}` 
    #     FOREIGN KEY ({linked_column}) REFERENCES {origin_table}({origin_column})
    #     ON UPDATE CASCADE ON DELETE CASCADE
    # """)
    cursor.close()

def drop_all_fks(conn):
    print("Dropping foreign keys")
    pbar = tqdm(total=len(foreign_keys))
    for fk in foreign_keys:
        drop_fk(conn,fk[0], fk[1])
        pbar.update(1)
    pbar.close()

def add_all_fks(conn):
    print("Creating foreign keys")
    pbar = tqdm(total=len(foreign_keys))
    for fk in foreign_keys:
        add_fk(conn, *fk)
        pbar.update(1)
    pbar.close()
