import mysql.connector
from tqdm import tqdm
import yaml

def create_variants_of_concern_table(config, voc):
    conn = mysql.connector.connect(**config)
    cursor = conn.cursor()

    cursor.execute("DROP TABLE IF EXISTS `temp_variants_of_concern`")

    cursor.execute("""CREATE TABLE`temp_variants_of_concern` (
  `nucleotide` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `amino_acid` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `msa` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;""")


    rows = 0
    with open(voc, "r") as f:
        for i,l in enumerate(f):
            pass
        rows = i
    print("Inserting data into variants of concern table")
    pbar = tqdm(total=rows)

    with open(voc, "r") as f:
        next(f) # skip header
        i=0
        data = []
        for line in f:
            i = i+1
            pbar.update(1)
            s = line.split('\t')
            nucleotide = s[0]
            amino_acid = s[1]
            msa = s[2]

            data.append((nucleotide, amino_acid, msa))

    statement = "INSERT INTO temp_variants_of_concern (nucleotide, " \
                  "amino_acid, msa) " \
                  "VALUES (%s, %s, %s)"

    cursor.executemany(statement, data)
    conn.commit()
    conn.close()
    pbar.close()

if __name__ == "__main__":
    import os
    workdir = ".."
    credentials = yaml.load(open("db_name.yml"), Loader=yaml.FullLoader)
    credentials["user"] = os.environ['MYSQL_USER']
    credentials["host"] = os.environ['MYSQL_HOST']
    credentials["password"] = os.environ['MYSQL_PW']
    voc = workdir + "/results/voc/voc.tsv"
    create_variants_of_concern_table(credentials,voc)
