import mysql.connector
from tqdm import tqdm
import pathlib

def create_position_table(config, positions):
    conn = mysql.connector.connect(**config)
    cursor = conn.cursor()

    cursor.execute("DROP TABLE IF EXISTS `temp_global_position`")

    cursor.execute("CREATE TABLE IF NOT EXISTS `temp_global_position`(first_case  VARCHAR(255) NOT NULL, "
                       "msa INT NOT NULL, PRIMARY KEY (msa))")

    rows = 0
    with open(positions, "r") as f:
        for i,l in enumerate(f):
            pass
        rows = i
    print("Inserting data into position table")
    pbar = tqdm(total=rows) # progress bar

    with open(positions, "r") as f:
        header = f.readline()

        data = []
        for line in f:
            pbar.update(1)
            pos = line.split('\t')
            data.append((pos[0], pos[1]))
            
    statement = "INSERT INTO temp_global_position (first_case, msa) VALUES(%s, %s)"
    cursor.executemany(statement, data)
    conn.commit()
    conn.close()
    pbar.close()
