import pandas as pd
from sqlalchemy import create_engine
import tempfile
import os

# This script converts sql databases created with the schema used before december 2021
# to the one used from december 2021 onwards.

# I recommend using it as an executable collection of SQL queries and not as a black
# box that magically does everything for you. It might be beneficial to step through it
# and try to understand the steps because depending on your data, some things might not work


def get_database_connection(database_name):
    user = os.environ['MYSQL_USER']
    ip = os.environ['MYSQL_HOST']
    pw = os.environ['MYSQL_PW']
    return create_engine(f'mysql+pymysql://{user}:{pw}@{ip}/{database_name}')


con = get_database_connection('test_germany_maps')

with con.begin() as con:
    con.execute("ALTER TABLE global_metadata DROP PRIMARY KEY")
    con.execute("ALTER TABLE global_metadata ADD strain_id INT")

    con.execute("""UPDATE global_metadata m SET m.strain_id = (
    #     SELECT i.strain_id FROM global_ID i WHERE i.strain = m.strain)""")
    con.execute("DELETE FROM global_metadata WHERE strain_id IS NULL")
    con.execute("ALTER TABLE global_metadata ADD PRIMARY KEY (strain_id)")


    con.execute("ALTER TABLE variants_of_concern DROP PRIMARY KEY")
    
    con.execute("ALTER TABLE variants_of_concern ADD id INT PRIMARY KEY NOT NULL auto_increment")


    df = pd.read_sql_table("sequence_voc", con=con)
    df = df.melt(id_vars='strain_id')

    df = df[df['value'] == 1]

    voc = pd.read_sql_table("variants_of_concern", columns=["id", "amino_acid"], con=con)


    merged = df.merge(voc, left_on="variable", right_on="amino_acid")
    merged = merged.drop(['variable','value', 'amino_acid'],1)
    merged = merged.rename(columns={'id': 'voc_id'})
    
    merged.to_sql('sequence_voc_new', con=con, if_exists='replace', index=False)
    # con.execute("CREATE INDEX seuence_voc_voc_IDX ON sequence_voc_new (`voc_id`)")    
    # con.execute("CREATE INDEX sequence_voc_strain_IDX ON sequence_voc_new (`strain_id`)")