import os
import mysql.connector
from tqdm import tqdm
import datetime
import code

def duplicate_database(credentials, small_database):
    u = credentials["user"]
    h = credentials["host"]
    pw = credentials["password"]
    db = credentials["database"]
    os.system(f"mysqldump -h {h} -u {u} -p{pw} {db} | mysql -h {h} -u {u} -p{pw} {small_database}")


def create_data_subset(credentials, small_database ,max_age_days):
    print('Creating small db with recent sequences')
    pbar = tqdm(total=4)
    if(small_database != credentials["database"]):
        duplicate_database(credentials, small_database)
    pbar.update(1)
    conn = mysql.connector.connect(user=credentials["user"],password=credentials["password"], database=small_database,host=credentials["host"])
    cursor = conn.cursor()

    cursor.execute('SELECT MAX(date) FROM global_metadata')
    for max_date in cursor:

        max_age = datetime.timedelta(days=max_age_days)
        cutoff_point = max_date- max_age
        


        cursor.execute("DELETE FROM global_metadata WHERE date < %s",(cutoff_point,))
        pbar.update(1)
        conn.commit()

        conn.close()
        pbar.close()