import mysql.connector
from tqdm import tqdm
import pathlib
import re

def create_statistics_table(date, config, raxml_log_path, bcftools_log_path):
    conn = mysql.connector.connect(**config)
    cursor = conn.cursor()

    cursor.execute("DROP TABLE IF EXISTS `temp_global_statistics`")

    cursor.execute("CREATE TABLE IF NOT EXISTS `temp_global_statistics`(date VARCHAR(10) NOT NULL, "
                       "taxa INT NOT NULL, sites INT NOT NULL, undetermined INT NOT NULL, "
                       "duplicates INT NOT NULL, partitions INT NOT NULL, patterns INT NOT NULL, "
                       "gaps DOUBLE NOT NULL, invariants DOUBLE NOT NULL, snps INT NOT NULL, "
                       "multiallelic_sites INT NOT NULL, multiallelic_snp_sites INT NOT NULL, PRIMARY KEY (date))")

    rows = 0
    print("Inserting data into statistics table")
    pbar = tqdm(total=1)

    raxml_file = open(raxml_log_path, "r")
    raxml = raxml_file.read()
    raxml_file.close()
    taxa = re.findall("alignment with (.*?) taxa|$", raxml)[0]
    sites = re.findall("taxa and (.*?) sites|$", raxml)[0]
    undetermined = re.findall("Fully undetermined columns found: (\w+)|$", raxml)[0]
    if not bool(undetermined):
        undetermined = 0
    duplicates = re.findall("Duplicate sequences found: (\w+)|$", raxml)[0]
    partitions = re.findall("comprises (.*?) partitions|$", raxml)[0]
    patterns = re.findall("partitions and (.*?) patterns|$", raxml)[0]
    gaps = re.findall("Gaps: (.*?) %|$", raxml)[0]
    invariants = re.findall("Invariant sites: (.*?) %|$", raxml)[0]
    bcftools_file = open(bcftools_log_path, "r")
    bcftools = bcftools_file.read()
    bcftools_file.close()
    snps = re.findall("(?<=number of SNPs:	).*|$", bcftools)[0]
    multiallelic_sites = re.findall("(?<=number of multiallelic sites:	).*|$", bcftools)[0]
    multiallelic_snp_sites = re.findall("(?<=number of multiallelic SNP sites:	).*|$", bcftools)[0]

    sql_query = []
    sql_query.append("INSERT INTO temp_global_statistics ")
    sql_query.append("(date, taxa, sites, undetermined, duplicates, partitions, patterns, gaps, invariants, snps, multiallelic_sites, multiallelic_snp_sites) ")
    sql_query.append("VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)")
    sql_query = ''.join(sql_query)
    cursor.execute(sql_query, (date, taxa, sites, undetermined, duplicates, partitions, patterns, gaps, invariants, snps, multiallelic_sites, multiallelic_snp_sites))
    pbar.update(1)

    conn.commit()
    conn.close()
    pbar.close()
