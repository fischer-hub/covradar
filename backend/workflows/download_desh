configfile: "configs/download_desh.yml"

workdir: config["workdir"]

from snakemake.remote.HTTP import RemoteProvider as HTTPRemoteProvider

HTTP = HTTPRemoteProvider()

rule all:
    input:
        "data/desh/desh.tsv.xz",
        "data/desh/desh.fasta.xz",

rule download_metadata:
    input:
        HTTP.remote(config["metadata"], keep_local=False)
    output:
        temp("data/desh/metadata.csv.xz")
    shell:
        """
        mv {input} {output}
        """

rule download_lineages:
    input:
        HTTP.remote(config["lineages"], keep_local=False)
    output:
        temp("data/desh/lineages.csv.xz")
    shell:
        """
        mv {input} {output}
        """

rule add_lineages:
    input:
        metadata = "data/desh/metadata.csv.xz",
        lineages = "data/desh/lineages.csv.xz"
    output:
        "data/desh/desh.tsv.xz"
    conda:
        "../envs/convert_desh_csv_to_tsv.yml"
    shell:
        """
        python scripts/convert_desh_csv_to_tsv.py \
        --metadata {input.metadata} \
        --lineage {input.lineages} \
        --tsv {output}
        """

rule download_sequences:
    input:
        HTTP.remote(config["sequences"], keep_local=False)
    output:
        "data/desh/desh.fasta.xz"
    shell:
        """
        mv {input} {output}
        """
