#!/usr/bin/env python3
"""
Creates from REF bases of tsv file built from VCF file and first sequence auf MSA a consensus sequence.
"""
import argparse
import pandas as pd
from Bio import SeqIO, AlignIO
from Bio.Alphabet import IUPAC, Gapped
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord

parser = argparse.ArgumentParser(description='Correctes reference and allele bases in tsv file containing the variants.')
parser.add_argument('vcf_tsv', type=str, help='Input tsv file which may contains errors')
#parser.add_argument('first_seq', type=str, help='Input tsv file which may contains errors')
parser.add_argument('msa', type=str, help='MSA file')
parser.add_argument('output', type=str, help='Filename of corrected tsv file')
args = parser.parse_args()

DATE = args.vcf_tsv.rsplit('/', 3)[1]

# position and reference base from tsv file
pos_ref = pd.read_csv(args.vcf_tsv, sep="\t")[["POS", "REF"]].values

# work around for getting first sequence, until issue #43 is fixed
# import MSA and extract first sequence
first = True
alignment = AlignIO.read(open(args.msa), "fasta", alphabet=Gapped(IUPAC.ambiguous_dna, "-"))
for record in alignment:
    if first:
        first_sequence = record.seq.upper().tomutable()
        first = False
    break
    
# import first sequence of MSA
# for record in SeqIO.parse(args.first_seq, "fasta", alphabet=Gapped(IUPAC.ambiguous_dna, "-")):
#     first_sequence = record.seq.upper().tomutable()

# merge pos_ref with first sequence for consensus sequence
consensus_seq = first_sequence

for row in pos_ref:
    consensus_seq[row[0]-1] = row[1]  # POS is 1 based, list is 0 based

# save consensus
record = SeqRecord(consensus_seq.toseq(), id=DATE, name="consensus sequence", description="consensus sequence built on " + DATE)
SeqIO.write(record, args.output, 'fasta')