#!/usr/bin/env python3
"""Computes consensus sequences per calendar week."""
import argparse
import sys
from argparse import Namespace
from collections import Counter
from typing import TextIO

import numpy as np
import screed


def parse_args(args: list) -> Namespace:
    """
    Parse a list of arguments.

    Parameters
    ----------
    args : list
        Arguments to parse.

    Returns
    -------
    _ : Namespace
        Parsed arguments.
    """
    parser = argparse.ArgumentParser(description="Splits data per day or week")
    parser.add_argument(
        "--input",
        type=str,
        required=True,
        help="Input FASTA file containing the sequences",
    )
    parser.add_argument(
        "--output",
        type=str,
        required=True,
        help="Output FASTA file to write consensus",
    )
    parser.add_argument(
        "--region",
        type=str,
        required=True,
        help="Region where the sequences come from",
    )
    return parser.parse_args(args)


def split(sequence: str) -> np.ndarray:
    """
    Split a sequence into an array containing base pairs.

    Parameters
    ----------
    sequence : str
        DNA sequence.

    Returns
    -------
    split_sequence : np.ndarray
        DNA sequence split into an array.
    """
    split_sequence = np.array([base for base in sequence])
    return split_sequence


def load_sequences(path: str) -> np.ndarray:
    """
    Load sequences in RAM.

    Parameters
    ----------
    path : str
        Path to FASTA file.

    Returns
    -------
    sequences : np.ndarray
        DNA sequences.
    """
    with screed.open(path) as seqfile:
        sequences = np.array(
            [split(str(record.sequence).upper()) for record in seqfile]
        )
    return sequences


def write_sequence(
    output: TextIO, identifier: str, sequence: str, line_length: int = 80
) -> None:
    """
    Write a sequence to a file.

    Parameters
    ----------
    output : TextIOWrapper
        File where sequence will be written.
    identifier : str
        Identifier of the genome.
    sequence : str
        Sequence of the genome.
    line_length : int, default=80
        Maximum length for each sequence line.
        Does not apply to identifier.
    """
    output.write(f">{identifier}\n")
    formatted_sequence = "".join(
        [
            f"{sequence[i:i + line_length]}\n"
            for i in range(0, len(sequence), line_length)
        ]
    )
    output.write(f"{formatted_sequence}")


def compute_consensus(sequences: np.ndarray) -> str:
    """
    Compute consensus sequence.

    Parameters
    ----------
    sequences : np.ndarray
        Sequences to compute consensus.

    Returns
    -------
    consensus : str
        Consensus sequence.
    """
    consensus = [""] * sequences.shape[1]
    for col in range(sequences.shape[1]):
        base_count = Counter(sequences[:, col])
        # A is default in case of ties
        consensus[col] = "A"
        for base in ["C", "G", "T", "-"]:
            if base_count[base] > base_count[consensus[col]]:
                consensus[col] = base
        # If column only contains something other than
        # A, C, G, T or - then an N is inserted
        if base_count[consensus[col]] == 0:
            consensus[col] = "N"
    consensus = "".join(consensus)
    return consensus


def get_identifier(region: str, input_file: str):
    """
    Return the identifier of the consensus sequence.

    Parameters
    ----------
    region : str
        Region where the sequences come from.
    input_file : str
        Path of the input file.

    Returns
    -------
    identifier : str
        Identifier of the consensus sequence.
    """
    suffix = input_file.split("/")[-1]
    prefix = suffix.replace(".fasta", "")
    identifier = f"{region}_{prefix}"
    return identifier


def main():  # pragma: no cover
    """Computes consensus sequences per calendar week."""
    args = parse_args(sys.argv[1:])
    sequences = load_sequences(args.input)
    consensus = compute_consensus(sequences)
    with open(args.output, "w") as output:
        identifier = get_identifier(args.region, args.input)
        write_sequence(output, identifier, consensus)


if __name__ == "__main__":
    main()  # pragma: no cover
