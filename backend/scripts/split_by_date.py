#!/usr/bin/env python3
"""Splits data per day or week."""
import argparse
import sys
from argparse import Namespace
from typing import TextIO

import pandas as pd
import screed
from isoweek import Week


def parse_args(args: list) -> Namespace:
    """
    Parse a list of arguments.

    Parameters
    ----------
    args : list
        Arguments to parse.

    Returns
    -------
    _ : Namespace
        Parsed arguments.
    """
    parser = argparse.ArgumentParser(description="Splits data per day or week")
    parser.add_argument(
        "--sequences",
        type=str,
        required=True,
        help="Input FASTA file containing the sequences",
    )
    parser.add_argument(
        "--metadata",
        type=str,
        required=True,
        help="Input TSV file containing the metadata",
    )
    parser.add_argument(
        "--output-folder",
        type=str,
        required=True,
        help="Output folder to save the results",
    )
    parser.add_argument(
        "--frequency",
        type=str,
        required=True,
        help="Frequency to run analysis (daily or weekly)",
    )
    return parser.parse_args(args)


def load_metadata(path: str) -> pd.DataFrame:
    """
    Load metadata in RAM and sorts values by date.

    Parameters
    ----------
    path : str
        Path to metadata file.

    Returns
    -------
    metadata : pd.DataFrame
        Metadata.
    """
    metadata = pd.read_csv(path, sep="\t").fillna("")
    metadata.sort_values(by="date", inplace=True)
    metadata.reset_index(drop=True, inplace=True)
    return metadata


def load_sequences(path: str) -> dict:
    """
    Load sequences in RAM.

    Parameters
    ----------
    path : str
        Path to FASTA file.

    Returns
    -------
    sequences : dict
        Dictionary with key (ID) and value (sequence) pairs.
    """
    sequences = {}
    with screed.open(path) as seqfile:
        for record in seqfile:
            sequences[record.name] = record.sequence
    return sequences


def write_sequence(
    output: TextIO, identifier: str, sequence: str, line_length: int = 80
) -> None:
    """
    Write a sequence to a file.

    Parameters
    ----------
    output : TextIOWrapper
        File where sequence will be written.
    identifier : str
        Identifier of the genome.
    sequence : str
        Sequence of the genome.
    line_length : int, default=80
        Maximum length for each sequence line.
        Does not apply to identifier.
    """
    output.write(f">{identifier}\n")
    formatted_sequence = "".join(
        [
            f"{sequence[i:i + line_length]}\n"
            for i in range(0, len(sequence), line_length)
        ]
    )
    output.write(f"{formatted_sequence}")


def split_daily(metadata: pd.DataFrame, sequences: dict, output_folder: str) -> None:
    raise NotImplementedError("Split by date daily is not implemented yet")


def get_weeks(metadata: pd.DataFrame) -> pd.Series:
    """
    Compute iso week for dates.

    Parameters
    ----------
    metadata : pd.DataFrame
        Metadata with date column in the format "yyyy-mm-dd".

    Returns
    -------
    weeks : pd.Series
        Computed iso weeks.
    """
    weeks = pd.to_datetime(metadata["date"]).map(lambda x: str(Week.withdate(x)))
    return weeks


def split_weekly(metadata: pd.DataFrame, sequences: dict, output_folder: str) -> None:
    """
    Split metadata and sequences on a weekly basis.

    Parameters
    ----------
    metadata : pd.DataFrame
        Metadata with date column in the format "yyyy-mm-dd".
    sequences : dict
        Dictionary with key (strain) and value (DNA sequence).
    output_folder : str
        Output folder to write results to.
    """
    weeks = get_weeks(metadata)
    for current_week in weeks.unique():
        output_prefix = f"{output_folder}/{current_week}"
        week_metadata = metadata[weeks == current_week]
        week_metadata.to_csv(f"{output_prefix}.tsv", index=False, sep="\t")
        with open(f"{output_prefix}.fasta", "w") as fasta:
            for strain in week_metadata["strain"]:
                if strain in sequences:
                    write_sequence(fasta, strain, sequences[strain])


def main():  # pragma: no cover
    """Split datasets daily or weekly."""
    args = parse_args(sys.argv[1:])
    metadata = load_metadata(args.metadata)
    sequences = load_sequences(args.sequences)
    if args.frequency.lower() == "daily":
        split_daily(metadata, sequences, args.output_folder)
    elif args.frequency.lower() == "weekly":
        split_weekly(metadata, sequences, args.output_folder)
    else:
        raise ValueError(f"--frequency {args.frequency} is invalid")


if __name__ == "__main__":
    main()  # pragma: no cover
