# %%
import argparse
import pandas as pd
from collections import Counter
from Bio import AlignIO
from Bio.SeqRecord import SeqRecord
from Bio.Alphabet import IUPAC, Gapped

# %%
parser = argparse.ArgumentParser(description='Takes multi sequence alignment and convert header in to csv file.')
parser.add_argument('msa_path', type=str, help='Input FASTA file containing the multi sequence alignment of the extracted protein')
parser.add_argument('output_path', type=str, help='Output base frequency plot in PDF')
args = parser.parse_args()

msa_path = args.msa_path  # path to MSA data
output_path = args.output_path  # path to location where output will be saved

#%%
msa_path = "/Users/Alice/gitRepos/rki-spike-protein/data/msa.fasta"
# %%

alignment = AlignIO.read(open(msa_path), "fasta", alphabet=Gapped(IUPAC.ambiguous_dna, "-"))
descriptions = [record.description.split("/", 3) if Counter(record.description)["/"] == 4 else record.description.split("/", 4) for record in alignment]

for i, el in enumerate(descriptions):
    if len(el) == 4:
        el.insert(2, "human")
    
# %%
table_header = ["Sampling Date", "Upload Date", "Species", "Country", "ID"]
wanted_order = ["Country", "Sampling Date", "Upload Date", "Species", "ID"]
df = pd.DataFrame(descriptions, columns=table_header)
df = df.reindex(columns=wanted_order)

# %%
df.to_csv(output_path)