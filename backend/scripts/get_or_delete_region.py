#!/usr/bin/env python3
"""Script to get genomes and metadata according to specified region, or delete them."""
import argparse
import sys
from argparse import Namespace
from typing import TextIO

import numpy as np
import pandas as pd
import screed
from tqdm import tqdm


def parse_args(args: list) -> Namespace:
    """
    Parse a list of arguments.

    Parameters
    ----------
    args : list
        Arguments to parse.

    Returns
    -------
    _ : Namespace
        Parsed arguments.
    """
    parser = argparse.ArgumentParser(
        description="Get or delete genomes and metadata from the region specified"
    )
    parser.add_argument(
        "--genomes",
        type=str,
        required=True,
        help="Input FASTA file containing the genomes",
    )
    parser.add_argument(
        "--metadata",
        type=str,
        required=True,
        help="Input TSV file containing the metadata",
    )
    parser.add_argument(
        "--output-genomes",
        type=str,
        required=True,
        help="Output FASTA file containing the genomes",
    )
    parser.add_argument(
        "--output-metadata",
        type=str,
        required=True,
        help="Output TSV file containing the metadata",
    )
    parser.add_argument(
        "--region", type=str, required=True, help="Name of the region to filter files"
    )
    parser.add_argument(
        "--delete",
        default=False,
        action="store_true",
        help="Enables deletion of the sequences from the specified region",
    )
    parser.add_argument(
        "--progress",
        default=False,
        action="store_true",
        help="Enables displaying the progress",
    )
    return parser.parse_args(args)


def read_metadata(path: str, progress: bool) -> pd.DataFrame:
    """
    Read TSV file containing metadata.

    Parameters
    ----------
    path : str
        Path of the file.
    progress : bool
        True if progress is to be shown.

    Returns
    -------
    metadata : pd.DataFrame
        Metadata.
    """
    with tqdm(total=1, disable=not progress) as pbar:
        pbar.set_description("Loading metadata file")
        metadata = pd.read_csv(path, sep="\t", low_memory=False)
        pbar.update()
        return metadata


def match(country: str, region: str) -> bool:
    """
    Check if region and country match.

    Parameters
    ----------
    country : str
        Country specified in metadata.
    region : str
        Region that should be filtered.

    Returns
    -------
    _ : bool
        True if country and region match,
        False otherwise.
    """
    # If nan is found return false to avoid errors
    if not isinstance(country, str) or not isinstance(region, str):
        return False
    return country.lower() == region.lower()


def get_mask(metadata: pd.DataFrame, region: str, delete: bool, progress: bool) -> list:
    """
    Return mask with metadata that should be kept or skipped.

    Parameters
    ----------
    metadata : pd.DataFrame
        DataFrame with metadata.
        Must have a column called country.
    region : str
        Region that should be filtered.
    delete : bool
        True if the region must be deleted instead of filtered.
    progress : bool
        True if progress is to be shown.

    Returns
    -------
    mask : list
        Mask with metadata that should be kept (True),
        otherwise (False).
    """
    if region.lower() == "global":
        mask = np.array([True] * metadata.shape[0])
    else:
        tqdm.pandas(desc="Processing metadata file", disable=not progress)
        mask = np.array(
            metadata["country"].progress_apply(match, args=(region,)).tolist()
        )
    if delete:
        mask = np.logical_not(mask)
    return mask


def filter_metadata(metadata: pd.DataFrame, mask: list) -> pd.DataFrame:
    """
    Filter metadata according to mask.

    Parameters
    ----------
    metadata : pd.DataFrame
        DataFrame with metadata.
    mask : list
        Mask with rows that should be selected.

    Returns
    -------
    filtered_metadata : pd.DataFrame
        Filtered metadata.
    """
    filtered_metadata = metadata[mask]
    return filtered_metadata


def get_strains(metadata: pd.DataFrame) -> set:
    """
    Return all strains in the metadata.

    Parameters
    ----------
    metadata : pd.DataFrame
        DataFrame with metadata.

    Returns
    -------
    strains : set
        Set with all strains in the metadata.
    """
    strains = set(metadata["strain"].astype(str))
    return strains


def write_metadata(metadata: pd.DataFrame, path: str) -> None:
    """
    Write metadata to file.

    Parameters
    ----------
    metadata : pd.DataFrame
        DataFrame with metadata.
    path : str
        Path to store metadata.
    """
    metadata.to_csv(path, index=False, sep="\t")


def write_sequence(
    output: TextIO, identifier: str, sequence: str, line_length: int = 80
) -> None:
    """
    Write a sequence to a file.

    Parameters
    ----------
    output : TextIOWrapper
        File where sequence will be written.
    identifier : str
        Identifier of the genome.
    sequence : str
        Sequence of the genome.
    line_length : int, default=80
        Maximum length for each sequence line.
        Does not apply to identifier.
    """
    output.write(f">{identifier}\n")
    formatted_sequence = "".join(
        [
            f"{sequence[i:i + line_length]}\n"
            for i in range(0, len(sequence), line_length)
        ]
    )
    output.write(f"{formatted_sequence}")


def filter_genomes(genomes: str, output: TextIO, strains: set, progress: bool) -> None:
    """
    Filter genomes according to set with strains to keep.

    Parameters
    ----------
    genomes : str
        Path to input file containing genomes.
    output : str
        Path to write filtered genomes.
    strains : set
        Set with strains that should be kept.
    progress : bool
        True if progress is to be shown.
    """
    with screed.open(genomes) as seqfile:
        with tqdm(total=len(strains), disable=not progress) as pbar:
            pbar.set_description("Processing FASTA file")
            for record in seqfile:
                if record.name in strains:
                    write_sequence(output, record.name, record.sequence)
                    pbar.update()


def main():  # pragma: no cover
    """Split genomes and metadata by region."""
    args = parse_args(sys.argv[1:])
    metadata = read_metadata(args.metadata, args.progress)
    mask = get_mask(metadata, args.region, args.delete, args.progress)
    metadata = filter_metadata(metadata, mask)
    strains = get_strains(metadata)
    write_metadata(metadata, args.output_metadata)
    with open(args.output_genomes, "w") as filtered_genomes:
        filter_genomes(args.genomes, filtered_genomes, strains, args.progress)


if __name__ == "__main__":
    main()  # pragma: no cover
