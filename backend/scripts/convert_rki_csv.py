import pandas
import argparse

def convert(csv_file, tsv_file):
    df = pandas.read_csv(csv_file, sep=',')
    # add a column with name 'country' and value 'Germany'
    df['country'] = 'Germany'
    df.to_csv(tsv_file, sep='\t', index=False)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Convert a csv file to a tsv file')
    parser.add_argument('csv_file', type=str, help='csv file to convert')
    args = parser.parse_args()
    if not args.csv_file.endswith('.csv'):
        print("The file must be a csv file")
        exit(1)
    convert(args.csv_file, args.csv_file.replace('.csv', '.tsv'))