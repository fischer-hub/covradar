import argparse
import sys
sys.path.append('../..')
import pandas
from shared.residue_converter.nt_aa_translation import translate_aa_to_nt, translate_nt_seq_aa_seq
from shared.residue_converter.position_converter import aa2na_genome
from shared.residue_converter.data_loader import get_reference

# DISCLAIMER:
# The script does not support the conversion of insertions at the moment.
# Furthermore, mutations cause by multiple SNPS per codon are not supported (e.g. E484P -> G23012C+A23013C)
# Therefore, the results always need to be double-checked before finally updating the voc.tsv file

def read_input_file(path):
    with open(path, "r") as input_file:
        mutations = [mut.replace("\n", "") for mut in input_file.readlines()]
    return mutations

def change_deletion_format(deletion, gene, ref_sequence):
    start_aa = deletion.split("DEL")[1].split("/")[0]
    stop_aa = deletion.split("DEL")[1].split("/")[1]
    start_nt = aa2na_genome(int(start_aa), gene)[0]
    stop_nt = aa2na_genome(int(stop_aa), gene)[1]
    nt_seq = ref_sequence[start_nt-1:stop_nt]
    aa_seq = translate_nt_seq_aa_seq(nt_seq)
    new_deletion_pattern = aa_seq + start_aa + "-" + stop_aa + "del"
    return new_deletion_pattern


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Prints nt and aa mutation pattern in CovRadar's format")
    parser.add_argument('--input', type=str, metavar='STR', required=True, help='Input file containing one amino acid mutation per line')
    parser.add_argument('--output', type=str, metavar='STR', required=True, help="Output file containing the aa mutation and nt mutation in CovRadar's format separated by tabs")
    parser.add_argument('--gene', metavar='STR', help="gene name (Default: S)", type=str, default="S")
    parser.add_argument('--nt_possible_mutations_distance', metavar='STR', help="maximum Hamming distance between the reference codon and possible mutated codons (Default: 1)", type=str, default=1)
    parser.add_argument('--ref_path', metavar='STR', help="FASTA file containing the reference sequence (uses SARS-CoV-2 as a reference by default, NC_045512.2)", type=str, default=None)
    parser.add_argument('--gff_path', metavar='STR', help="GFF file containing the genome annotation (uses SARS-CoV-2 as a reference by default, NC_045512.2)", type=str, default=None)
    args = parser.parse_args()

    ref_sequence = get_reference(path=args.ref_path)

    aa_mutations = read_input_file(args.input)

    aa_mutations = [change_deletion_format(mut, args.gene, ref_sequence) if mut[0:3]=="DEL" else mut for mut in aa_mutations]

    nt_mutations = [translate_aa_to_nt(aa_mut, args.gene, args.nt_possible_mutations_distance, args.ref_path, args.gff_path).nt_possible_mutations for aa_mut in aa_mutations]

    nt_mutations_final = []
    for item in nt_mutations:
        if 'other mutations possible' in item:
            item.remove('other mutations possible')
        if len(item) > 1:
            nt_mut = "/".join(item)
        else:
            nt_mut = item[0]
        nt_mutations_final.append(nt_mut)

    df = pandas.DataFrame(data={"nucleotide":nt_mutations_final, "amino acid": aa_mutations})
    df.to_csv(args.output, index=False, sep="\t")