#!/usr/bin/env python3

from Bio import SeqIO
import pandas as pd
import argparse

parser = argparse.ArgumentParser(description='Separates all proteins from a FASTA file')
parser.add_argument('input', type=str, help='Input FASTA file containing the proteins')
parser.add_argument('output', type=str, help='Output folder to save the separated proteins')
args = parser.parse_args()

dictionary = {}
for record in SeqIO.parse(args.input, "fasta"):
    protein = record.id.split("|")[0]
    if protein in dictionary:
        dictionary[protein].append(record)
    else:
        dictionary[protein] = [record]

for key in dictionary:
    SeqIO.write(dictionary[key], args.output + "/" + key + ".fasta", 'fasta')
