#!/usr/bin/env python3
"""
Takes multi sequence alignment and domain information.
Plots abundance of base frequencies not equal to consensus sequence and
highlights the domains.
"""
import math
import datetime
import matplotlib
from matplotlib import cm
import numpy as np
import pandas as pd
import plotly.graph_objects as go
import argparse

from matplotlib.patches import Rectangle, Polygon
from collections import defaultdict, Counter

def allele_freq(vcf_data, spike_len):
    # -1 skip
    # 1 is reference
    # 0 is allele
    alt_ref_relation = {'A': {'A': 1, 'C': 0, 'T': 0, 'G': 0, '-': 0, 'N': -1},
                        'C': {'A': 0, 'C': 1, 'T': 0, 'G': 0, '-': 0, 'N': -1},
                        'T': {'A': 0, 'C': 0, 'T': 1, 'G': 0, '-': 0, 'N': -1},
                        'G': {'A': 0, 'C': 0, 'T': 0, 'G': 1, '-': 0, 'N': -1},
                        '-': {'A': 0, 'C': 0, 'T': 0, 'G': 0, '-': 1, 'N': -1},
                        'W': {'A': -1, 'C': 0, 'T': -1, 'G': 0, '-': 0, 'N': -1},
                        'S': {'A': 0, 'C': -1, 'T': 0, 'G': -1, '-': 0, 'N': -1},
                        'M': {'A': -1, 'C': -1, 'T': 0, 'G': 0, '-': 0, 'N': -1},
                        'K': {'A': 0, 'C': 0, 'T': -1, 'G': -1, '-': 0, 'N': -1},
                        'R': {'A': -1, 'C': 0, 'T': 0, 'G': -1, '-': 0, 'N': -1},
                        'Y': {'A': 0, 'C': -1, 'T': -1, 'G': 0, '-': 0, 'N': -1},
                        'N': {'A': -1, 'C': -1, 'T': -1, 'G': -1, '-': 0, 'N': -1},
                        'B': {'A': 0, 'C': -1, 'T': -1, 'G': -1, '-': 0, 'N': -1},
                        'D': {'A': -1, 'C': 0, 'T': -1, 'G': -1, '-': 0, 'N': -1},
                        'H': {'A': -1, 'C': -1, 'T': -1, 'G': 0, '-': 0, 'N': -1},
                        'V': {'A': -1, 'C': -1, 'T': 0, 'G': -1, '-': 0, 'N': -1}}

    # ini
    freq_list = np.zeros(spike_len, dtype=float)

    for pos_i, pos in enumerate(vcf_data["POS"]):
        seqs = vcf_data.columns[8:]
        tot = len(seqs)
        no_seqs = 0
        alt_list = [a.upper() for a in vcf_data["ALT"].iloc[pos_i].split(",")]
        ref_alt_dict = { i:j for i,j in zip(range(1,len(alt_list)+1), alt_list)}
        ref_alt_dict[0] = vcf_data["REF"].iloc[pos_i].upper()

        alts = Counter(vcf_data[seqs].iloc[pos_i])

        for a_i in alts:
            if a_i != 0:
                a = ref_alt_dict[a_i]
                ref = ref_alt_dict[0]
                rel = alt_ref_relation[a][ref]
                if rel == 0:  # allele
                    no_seqs += alts[a_i]
                elif rel == -1:  # ambiguous
                    # exclude sequences
                    tot -= alts[a_i]  
        freq = 0 if tot == 0 else float(no_seqs/tot)
        freq_list[pos-1] = freq # position is 1 based
    return freq_list

# for visualization I'm converting the 1D array to 2D
def chunks(lst, n):
    """
    # for visualization I'm converting the 1D array to 2D
    Takes 1D list lst and number n and splits list after n elements
    Fills last segment with -1 if its length is not n
    output: 2D list
    """
    result = []
    for i in range(0, len(lst), n):
        chunk = lst[i:i+n]
        result.append(list(chunk))
        if len(chunk) < n:
            for i in range(n - len(chunk)):
                result[-1].append(None)
    return result

def create_plot(vcf_data, protein_length, domains):

    freq_list = allele_freq(vcf_data, protein_length)

    # this will calculate the optimal length of the matrix
    row_len = math.ceil(math.sqrt(len(freq_list)))
    # converted 2D array
    nested_array = chunks(freq_list, row_len)[::-1]

    # plotting
    # This calculates the color scale
    tab20c_r_cmap = matplotlib.cm.get_cmap('tab20c_r')
    tab20c_r = []
    norm = matplotlib.colors.Normalize(vmin=0, vmax=255)

    for i in range(0, 256):
        if i == 0:
            tab20c_r.append([norm(i), "rgb(250, 250, 245)"]) # zero to 1/3000 get different color, rgb(250, 250, 245)
        else:
            k = matplotlib.colors.colorConverter.to_rgb(tab20c_r_cmap(norm(i)))
            tab20c_r.append([norm(i), "rgb"+str(k)])

    no_rows = math.floor(len(freq_list) / row_len)+1
    y_axis_labels = [(i*row_len)+start_pos for i in range(no_rows)]
    len_y_axis_labels = len(y_axis_labels)

    fig = go.Figure(data=go.Heatmap(
                        z=nested_array,
                        xgap=1.2, ygap=1.2,
                        colorscale=tab20c_r,
                        zmin=0.00001, zmax=1.,
                        ),
                    )
    x_axis_template = dict(
            showticklabels = False, 
            tickmode = "linear",
            tick0 = 0,
            dtick = 1,
            showspikes = True,
            )
    y_axis_template = dict(
            tickmode = 'array',
            tickvals = list(range(len_y_axis_labels)),
            ticktext = list(reversed(y_axis_labels)),
            tickfont={"size":5},
            showspikes = True,
            )

    fig.update_layout(
        xaxis = x_axis_template,
        yaxis = y_axis_template,
        width = 700, height = 700,
        autosize = True,
        plot_bgcolor='rgba(0,0,0,0)' # no background color
        )

    # domain boxes
    for ind, dom in enumerate(domains):
        name = dom[0]
        start_dom = dom[1][0]
        end_dom = dom[1][1] + 1

        res_start = list(filter(lambda i: i <= start_dom, y_axis_labels[::-1]))[0]
        y_box_start = y_axis_labels.index(res_start)
        x_box_start = start_dom - res_start

        res_end = list(filter(lambda i: i <= end_dom, y_axis_labels[::-1]))[0]
        y_box_end = y_axis_labels.index(res_end)
        x_box_end = end_dom - res_end

        if y_box_start == y_box_end:
            nodes = [(x_box_start, len_y_axis_labels-1-y_box_start), (x_box_end, len_y_axis_labels-1-y_box_end), (x_box_end, len_y_axis_labels-1-y_box_end-1), (x_box_start, len_y_axis_labels-1-y_box_end-1)]
        else:
            nodes = [(x_box_start-0.5, len_y_axis_labels-1-y_box_start+0.5),(row_len-0.5, len_y_axis_labels-1-y_box_start+0.5),(row_len-0.5, len_y_axis_labels-1-y_box_end+0.5),(x_box_end-0.5, len_y_axis_labels-1-y_box_end+0.5),
                    (x_box_end-0.5, len_y_axis_labels-1-y_box_end-1+0.5),(0-0.5, len_y_axis_labels-1-y_box_end-1+0.5), (0-0.5, len_y_axis_labels-1-y_box_start-1+0.5), (x_box_start-0.5, len_y_axis_labels-1-y_box_start-1+0.5)]
        domain_box_svg_path = str(nodes).replace(" ", "").replace("(", "L").replace("[L", " M ").replace("),", ", ").replace(")]", " Z").replace(", ", " ")
        fig.update_layout(
            shapes=[
                # Polygon
                dict(
                    type="path",
                    path=domain_box_svg_path,
                    line_color="LightSeaGreen",
                    fillcolor=None,
                ),
            ],
        )
    return fig
    

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Takes multi sequence alignment and domain information. Plots abundance of base frequencies not equal to consensus sequence and highlights the domains.')
    parser.add_argument('snp_vcf_path', type=str, help='Input VCF file')
    parser.add_argument('numbering', type=str, help='Input TSV file containing the base positions of the MSA and the corresponding positions of the first case sequence')
    parser.add_argument('output_path', type=str, help='Output base frequency plot in PDF')
    args = parser.parse_args()

    # input
    snp_path = args.snp_vcf_path  # path to SNP data
    position_path = args.numbering # path to position table for numbering
    output_path = args.output_path  # path to location where output will be saved

    start_pos = 1  # sequence positions are 1 based, not 0 based

    # TSV file built from VCF
    vcf_data = pd.read_csv(snp_path, sep="\t", skiprows=2)
    # protein length in bases
    position_table = pd.read_csv(position_path, sep="\t", dtype={"msa":int, "reference":str})
    protein_length = position_table.iloc[-1,:]["msa"]  # in bases 

    rbd = position_table.loc[position_table['reference'].isin(["955","1623"])] # RBD domain of first case (NC_045512)
    domains = [("RBD", [rbd.iloc[0]["msa"], rbd.iloc[1]["msa"]])]

    fig = create_plot(vcf_data, protein_length, domains)

    fig.write_image(output_path, scale=2)