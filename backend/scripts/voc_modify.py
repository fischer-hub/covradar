import sys
sys.path.append('../') 
from shared.residue_converter.position_converter import na_genome_to_gene
import re
import pandas as pd
import argparse
from regex_engine import generator


def voc_modify(voc_path,position_path):
    """Takes position table and the VOC table path and returns a new voc table
       with msa column added

    Args:
        voc_path (str): Path to VOC file
        position_path (str): Path to position table
        voc_modified_path
    Returns:
        pandas dataframe: modified VOC table with msa column
    """
    position_table = pd.read_csv(position_path, sep='\t',dtype={'reference': str,'msa':int})
    voc_data = pd.read_csv(voc_path, sep="\t")
    voc_data_modified = pd.DataFrame(columns=["nucleotide","amino acid","msa"])

    for i, row in voc_data.iterrows():
        nucleotide = row["nucleotide"]
        amino_acid = row["amino acid"]
        nt_mutations = re.split(",|\\+",nucleotide)
        msa_positions = []
        for nt_mutation in nt_mutations:
            msa_positions.append(na_genome_mut_to_na_msa(nt_mutation,position_table))
            
        msa = ",".join(msa_positions)

        voc_data_modified.loc[i] = [nucleotide, amino_acid, msa]

    return voc_data_modified  


def na_genome_mut_to_na_msa(nt_mutation,glob_pos_table):
    # input nt mutation and glob_pos_table with spike and msa pos
    # nt_mutation contains nt genome position
    # accepted formats C21614T, del:21767:6, ins:22873:6:TACTAC 
    # 
    # output na position in msa
    
    # extract nt genome position
    pattern = "(?<=\w|:)\d{5}(?=\w|:)"
    nt_pos = re.findall(pattern,nt_mutation)
    if len(nt_pos) == 0 or len(nt_pos) > 1 + len(re.findall("/",nt_mutation)):
        print("invalid mutation format in " + nt_mutation)
        return None
    nt_pos = int(nt_pos[0])

    # map to spike pos and then to msa
    spike_pos = na_genome_to_gene(nt_pos)
    index = glob_pos_table["reference"] == str(spike_pos)
    msa = glob_pos_table.loc[index,"msa"].values[0]
    msa = str(msa)


    # special cases for insertion, deletions
    if "ins" in nt_mutation: 
        mt_length = re.findall("(?<=\d{5}:)\d+(?=:)",nt_mutation)[0]
        # check if ins already in glob_pos_table
        generate = generator()
        range_after_dot = generate.numerical_range(1,int(mt_length))[1:]
        regex_pattern = "(%s)[.]%s" % (spike_pos, range_after_dot)
        index_ins = glob_pos_table["reference"].str.match(pat=regex_pattern)
        msa_ins = glob_pos_table.loc[index_ins ,"msa"]
        if len(msa_ins) == int(mt_length):
            msa = str(int(msa) + 1) + ":" + str(int(msa) + int(mt_length))
        else:
            msa = "None"           
    elif "del" in nt_mutation:
        mt_length = re.findall("(?<=\d{5}:)\d+",nt_mutation)[0]
        msa = msa + ":" + str(int(msa) + int(mt_length) - 1)


    return msa


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Takes position table and the VOC table path and returns a new voc table \
       with msa column added.')
    parser.add_argument('voc_path', type=str, help='Input TSV file containing the variants of \
        concern')
    parser.add_argument('numbering', type=str, help='Input TSV file containing the base positions \
        of the MSA and the corresponding positions of the first case sequence')
    parser.add_argument('output_path', type=str, help='Output modified VOC')
    args = parser.parse_args()

    voc_table_modified = voc_modify(args.voc_path, args.numbering)

    voc_table_modified.to_csv(args.output_path, sep="\t", index=False)
