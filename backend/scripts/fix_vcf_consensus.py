#!/usr/bin/env python3

import numpy as np
import argparse

parser = argparse.ArgumentParser(description='Correctes reference and allele bases in tsv file containing the variants.')
parser.add_argument('input', type=str, help='Input tsv file which may contains errors')
parser.add_argument('output', type=str, help='Filename of corrected tsv file')
args = parser.parse_args()

with open(args.input, 'r') as f, open(args.output, 'w') as t, open(args.output.rsplit('/', 1)[0]+"/log_corrupted_positions.txt", 'a') as log:

    t.write(f.readline())

    #lines = f.readlines()
    #lines[716] # this is the line with problem on tsv file of 01-07-2020

    l = 0
    for line in f:
        l = l + 1
        #print('reading line number: ' + str(l))

        mutation_array = np.array(line.split('\t')[9:])
        mutation_array[-1] = mutation_array[-1][0] # remove \n in last element

        (value, counts) = np.unique(mutation_array, return_counts=True)
        value_of_most_counts = int(value[np.argmax(counts)])

        #print('value of most counts: ' + str(value_of_most_counts))

        if value_of_most_counts != 0 :

            print('Line ' + str(l) + ' needs to fix REF and ALT alleles  ')
            log.write(str(l))

            before_ref_column = "\t".join(line.split('\t')[:3])
            ref = line.split('\t')[4].split(',')[value_of_most_counts - 1]

            s = line.split('\t')[4].split(',')
            s[value_of_most_counts - 1] = line.split('\t')[3]
            alt = ",".join(s)

            after_alt_column = "\t".join(line.split('\t')[5:9])

            new_mutation_array = mutation_array
            index_ref = np.where(mutation_array == str(value_of_most_counts))
            index_zero = np.where(mutation_array == '0')
            new_mutation_array[index_ref[0]] = '0'
            new_mutation_array[index_zero[0]] = str(value_of_most_counts)

            after_format_column = "\t".join(new_mutation_array)

            t.write(before_ref_column + '\t' + ref + '\t' + alt + '\t' + after_alt_column + '\t' + after_format_column + '\n' )

        else:
            #print('writing line number: ' + str(l))
            t.write(line)







