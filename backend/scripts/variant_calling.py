#!/usr/bin/env python
import numpy as np
from Bio import AlignIO, SeqIO
import argparse
from os import listdir
from os.path import isfile, join
from pathlib import Path
import ray
import gzip

def load_position_columns(msa):
    align = AlignIO.read(msa, "fasta")
    sequences = np.array([np.array(list(record.seq)) for record in align])
    samples = np.array([record.id for record in align])
    bases_by_position = np.transpose(sequences)
    return bases_by_position, samples

def vcf_table_header(msa, first_case):
    first = SeqIO.read(first_case, "fasta").seq
    bases_by_position, samples = load_position_columns(msa)
    header = "##fileformat=VCFv4.2\n"
    vcf_header = header + "##contig=<ID=1,length="+str(len(first))+">\n"
    table_header = np.append(np.array(["#CHROM", "POS", "ID", "REF", "ALT", "QUAL", "FILTER", "INFO"]), samples)
    header_rows = vcf_header + "\t".join(table_header)
    return str(first), [(i,j) for i,j in enumerate(bases_by_position)], header_rows

@ray.remote
def vcf_row_generation(first, position_bases):
    position = position_bases[0]
    bases = position_bases[1]
    ref = first[position]
    set_bases = set(bases)
    if set(ref) != set_bases:
        # this determines the ALT numbers in the VCF file
        base_no = {ref:0}
        alt = sorted(list(l for l in set(bases) if l != ref))
        for i,b in enumerate(alt):
            base_no[b] = i+1
        
        def f(x):
            return base_no[x]
        # assign base to ALT number
        mapped = np.array(list(map(f, bases)))
        # prefix "#CHROM", "POS", "ID", "REF", "ALT", "QUAL", "FILTER", "INFO"
        prefix = np.array(["1", str(position+1), ".", ref, ",".join(alt), ".", ".", "."])
        vcf_table = np.append(prefix, mapped)
        return vcf_table


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='calls variants and stores them in a TSV table')
    parser.add_argument('threads', type=int, help='Number of threads.')
    parser.add_argument('msa', type=str, help='Input FASTA file containing MSA')
    parser.add_argument('first_case', type=str, help='Input FASTA file containing first case sequence mapped to MSA')
    parser.add_argument('vcf', type=str, help='Output TSV table containing variants and positions')

    args = parser.parse_args()
    
    ray.init()

    # create vcf header
    first, bases_by_position, vcf_header = vcf_table_header(args.msa, args.first_case)

    # create rows
    row_part_ids = []
    for el in bases_by_position:
        row_part_ids.append(vcf_row_generation.remote(first, el))

    # save 
    with gzip.open(args.vcf, 'ab') as f:
        np.savetxt(f, [], delimiter='\t', fmt='%s', header=vcf_header, comments="", newline='\n')

        for rid in row_part_ids:
            rows_part = ray.get(rid)
            if rows_part is not None:
                np.savetxt(f, [rows_part], delimiter='\t', fmt='%s', comments="", newline='\n')

