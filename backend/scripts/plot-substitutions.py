#!/usr/bin/env python3

import matplotlib.pyplot as plt
import matplotlib as mpl
import argparse
import re

parser = argparse.ArgumentParser(description='Creates a svg plot with substitutions identified by BCFtools')
parser.add_argument('bcftools', type=str, help='BCFtools log')
parser.add_argument('output', type=str, help='Output SVG file')
args = parser.parse_args()

bcftools_file = open(args.bcftools, "r")
bcftools = bcftools_file.read()
bcftools_file.close()
ac = int(re.findall("(?<=A>C	).*|$", bcftools)[0])
ag = int(re.findall("(?<=A>G	).*|$", bcftools)[0])
at = int(re.findall("(?<=A>T	).*|$", bcftools)[0])
ca = int(re.findall("(?<=C>A	).*|$", bcftools)[0])
cg = int(re.findall("(?<=C>G	).*|$", bcftools)[0])
ct = int(re.findall("(?<=C>T	).*|$", bcftools)[0])
ga = int(re.findall("(?<=G>A	).*|$", bcftools)[0])
gc = int(re.findall("(?<=G>C	).*|$", bcftools)[0])
gt = int(re.findall("(?<=G>T	).*|$", bcftools)[0])
ta = int(re.findall("(?<=T>A	).*|$", bcftools)[0])
tc = int(re.findall("(?<=T>C	).*|$", bcftools)[0])
tg = int(re.findall("(?<=T>G	).*|$", bcftools)[0])

dat = [
	[0,'A>C',ac],
	[1,'A>G',ag],
	[2,'A>T',at],
	[3,'C>A',ca],
	[4,'C>G',cg],
	[5,'C>T',ct],
	[6,'G>A',ga],
	[7,'G>C',gc],
	[8,'G>T',gt],
	[9,'T>A',ta],
	[10,'T>C',tc],
	[11,'T>G',tg],
]

mpl.use('Agg')
fig = plt.figure(figsize=(4.33070866141732,3.93700787401575))
cm  = mpl.cm.get_cmap('winter')
n = 12
col = []
for i in list(range(n)): col.append(cm(1.*i/n))
ax1 = fig.add_subplot(111)
ax1.bar([row[0] for row in dat], [row[2] for row in dat], color=col)
ax1.set_ylabel('Count')
ax1.ticklabel_format(style='plain', axis='y')
ax1.set_xlim(-0.5,n+0.5)
plt.xticks([row[0] for row in dat],[row[1] for row in dat],rotation=45)
plt.title('Substitution Types')
plt.savefig(args.output)
plt.close()
