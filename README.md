![](https://img.shields.io/badge/snakemake-4.5+-darkgreen)
![](https://img.shields.io/badge/uses-conda-brightgreen.svg)
![](https://img.shields.io/badge/uses-docker-blue.svg)
<!--![](https://img.shields.io/badge/uses-singularity-yellow.svg)-->
<!--![](https://img.shields.io/badge/licence-GPL--3.0-lightgrey.svg)-->

[![Generic badge](https://img.shields.io/badge/Run-WebApp-purple.svg)](https://covradar.net)
<!--[![Generic badge](https://img.shields.io/badge/OUP-Bioinformatics-red.svg)]()-->
<!--[![Generic badge](https://img.shields.io/badge/Wiki-available-purple.svg)](https://gitlab.com/RKIBioinformaticsPipelines/ncov_minipipe/-/wikis/home)-->

<!--[![Twitter Follow](https://img.shields.io/twitter/follow/rki_de.svg?style=social)](https://twitter.com/rki_de)-->

# CovRadar

Continuously tracking and filtering SARS-CoV-2 mutations

# Table of Content
- [CovRadar - The pipeline](#covradar_pipeline)
    - [Installation](#pipeline_installation)
    - [Variants Of Concern (VOC) File](#pipeline_voc)
    - [Running](#pipeline_running)
- [Covradar - The app](#covradar_app)
    - [Domains](#app_domains)
    - [Installation - The manual way](#app_installation_manual)
        - [Start the app local](#app_start_local)
    - [Installation - The Docker way](#app_installation_docker)
        - [Using Docker compose](#app_docker_compose)
        - [Using an existing MySQL database](#app_docker_existing_mysql)
        - [Use your own data](#app_docker_own_data)
- [Additional Analysis Scripts](#analysis_scripts)
    - [Nucleotide and Amino Acid Translations + Position Conversions](#translations_conversions)
    - [Vizualization of Nucleotide and Amino Acid Frequencies (without using the app)](#vizualize_withou_app)
    - [Gene Extraction](#gene_extraction)


# CovRadar - The pipeline <a name="covradar_pipeline"></a>

## Installation <a name="pipeline_installation"></a>

To copy the contents of this repository, run the following command:

```
git clone git@gitlab.com:dacs-hpi/covradar.git covradar
```

This pipeline requires a Linux system and conda to manage all of its dependencies. Please, install conda on your machine beforehand (https://www.anaconda.com/products/individual).

With conda installed, we can create a new environment with Snakemake and all the other pipeline dependencies by running the following command:

```
cd covradar/backend
conda env create -n covradar_pipeline --file envs/covradar_pipeline.yml
```

For some reason Snakemake cannot automatically activate conda environments, so you need to copy the activate binary from your main conda folder to the environment you just created. For example:

```
cp ~/anaconda3/bin/activate ~/anaconda3/envs/covradar_pipeline/bin/activate
```

It is possible to modify the config file with the appropriate parameters and samples:

```
nano config.yaml
```

The most important parameters to modify are:
* **workdir** - the current directory where you downloaded the pipeline, which is the output of the command `pwd`, e.g., **/home/fabio/Desktop/covradar/backend**;
* **threads** - for the most time consuming steps of the pipeline, the threads can be set individually here;
* **regions** - list of individual countries available in your metadata or Global for all countries;
* **outputs** - which can be *pdf* reports, *website* or both;
* **frequency** - it can create **weekly** or **daily** reports;
* **samples** - here you can list one or more samples to be processed.

Alternatively, you can run the script create_config.py to automatically update the config file. It only requires the datasets to be subfolders inside data. For example, to process the German samples from Charité we can run the following commands to download the dataset:

```
mkdir data/charite
wget -O data/charite/charite-SARS-CoV-2.fasta.gz https://civnb.info/public/charite-SARS-CoV-2.fasta.gz
wget -O data/charite/charite-SARS-CoV-2.tsv.gz https://civnb.info/public/charite-SARS-CoV-2.tsv.gz
```

Then we can run `python create_config.py`, which will update the samples and regions to:

```
samples:
    charite:
        genomes: 'data/charite/charite-SARS-CoV-2.fasta.gz'
        metadata: 'data/charite/charite-SARS-CoV-2.tsv.gz'

regions: ['Germany']
```

It is also possible to add third party data to be processed. For example, to process GISAID data, we can put it in the samples list and update the list of regions:

```
samples:
    gisaid:
        genomes: 'data/gisaid/sequences_2020-12-29_08-08.fasta.gz'
        metadata: 'data/gisaid/metadata_2020-12-29_08-01.tsv.gz'

regions: ["Global","Algeria","Andorra","Argentina","Armenia","Aruba","Australia","Austria",
          "Bahrain","Bangladesh","Belarus","Belgium","Belize","Benin","Bosnia and Herzegovina",
          "Botswana","Brazil","Brunei","Bulgaria","Burkina Faso","Cambodia","Cameroon","Canada",
          "Chile","China","Colombia","Costa Rica","Côte d'Ivoire","Croatia","Cuba","Curacao",
          "Cyprus","Czech Republic","Democratic Republic of the Congo","Denmark","Dominican Republic",
          "Ecuador","Egypt","El Salvador","Equatorial Guinea","Estonia","Finland","France","Gabon",
          "Gambia","Georgia","Germany","Ghana","Greece","Guadeloupe","Guam","Guatemala","Hong Kong",
          "Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland","Israel","Italy","Jamaica",
          "Japan","Jordan","Kazakhstan","Kenya","Kosovo","Kuwait","Latvia","Lebanon","Lithuania",
          "Luxembourg","Madagascar","Malaysia","Mali","Malta","Mexico","Moldova","Mongolia",
          "Montenegro","Morocco","Mozambique","Myanmar","Nepal","Netherlands","New Zealand","Nigeria",
          "North Macedonia","Norway","Oman","Pakistan","Palestine","Panama","Papua New Guinea","Peru",
          "Philippines","Poland","Portugal","Qatar","Republic of the Congo","Romania","Russia","Rwanda",
          "Saint Barthélemy","Saint Martin","Saudi Arabia","Senegal","Serbia","Sierra Leone","Singapore",
          "Slovakia","Slovenia","South Africa","South Korea","Spain","Sri Lanka","Suriname","Sweden",
          "Switzerland","Taiwan","Thailand","Timor-Leste","Trinidad","Tunisia","Turkey","Uganda",
          "Ukraine","United Arab Emirates","United Kingdom","Uruguay","USA","Venezuela","Vietnam",
          "Zambia","Zimbabwe"]
```

Lastly, it is also possible to combine different datasets. For example, to merge GISAID and Charité datasets and process them, we can simply add both of them to the samples list:

```
samples:
    charite:
        genomes: 'data/charite/charite-SARS-CoV-2.fasta.gz'
        metadata: 'data/charite/charite-SARS-CoV-2.tsv.gz'
    gisaid:
        genomes: 'data/gisaid/sequences_2020-12-09_07-43.fasta.gz'
        metadata: 'data/gisaid/metadata_2020-12-09_17-36.tsv.gz'
```

### Variants Of Concern (VOC) File <a name="pipeline_voc"></a>

You can add the path to your VOC table in config.yaml:

```
voc: 'data/voc.tsv'
```
It needs to be a TSV file with the following header:
```
nucleotide	amino acid
```
Please be aware of our mutation nomenclature:
| Mutation        | Nucleotide           | Amino Acid  |
| ------------- |:-------------:| -----:|
| SNP      | C21614T | L18F |
| Insertion      | ins:22861:3:TAC<br>ins:22873:6:TACTAC      |   Y433-434ins<br>YY437-438ins |
| Deletion | del:21767:6<br>del:21992:3      |    HV69-70del<br>Y144-144del |

Combination of mutations are also possible:

| Combination        | Nucleotide           | Amino Acid  |
| ------------- |:-------------:| -----:|
| Two SNPs on different positions leading to one amino acid change      | A22461G+G22462C | K300S |
| Two SNPs on the same position leading to one amino acid change      | G22132C/G22132T      |   R190S |
| VOC is more than one amino acid mutation | G22161C,C22311A      |    Y200C, T250N |


## Running <a name="pipeline_running"></a>

After a successful installation, we can activate the newly created environment and run the pipeline (please don't forget to modify the config file with your samples and parameters in advance).

```
conda activate covradar_pipeline
snakemake --use-conda --cores 28
```

## Creating a database from pipeline results <a name="pipeline_database"></a>

After a successful pipeline run, we can create MySQL tables for the frontend by navigating to backend/sql and executing
```
    # create environment
    conda env create -f backend/sql/environment.yml -n database
    conda activate database

    # create empty database
    mysql -u $MYSQL_USER -p $MYSQL_PW -h $MYSQL_HOST -e "create database DB_NAME;"

    # upload backend/results to database DB_NAME
    python create_spike_database.py -db DB_NAME
```
The first line has to be executed once. For info about options regarding database creation, use `python create_spike_database -h`
Note that the mysql server needs to permit loading data from files on the database client. This can be done by adding

```
[mysqld]
local-infile 

[mysql]
local-infile
```
to `/etc/mysql/my.cnf`

# CovRadar - The app <a name="covradar_app"></a>


## Domains <a name="app_domains"></a>

CovRadar is available at:
- https://covradar.net/
- https://covradar.de/


You can install the app manually or via Docker. 


## Installation - The manual way <a name="app_installation_manual"></a>

You can run the app without docker on a Linux system.

Clone Covradar

```
git clone https://gitlab.com/dacs-hpi/covradar.git covradar
```

Install the environment.

```
conda env create -f frontend/environment.yml -n app_env
conda activate app_env
```

Install Redis for caching.
```
sudo apt install redis-server
sudo service redis-server {start | stop | status}
```

Test successful Redis installation
```
redis-cli
```
Type 'ping':  127.0.0.1:6379> ping  
output will be PONG  
leave Redis: 127.0.0.1:6379> exit

Add REDIS_CACHE_URL to .bashrc file:
```
export REDIS_CACHE_URL=redis://localhost:6379/0
```

Install MySQL: https://www.mysqltutorial.org/install-mysql/

Create a MySQL user: https://www.digitalocean.com/community/tutorials/how-to-create-a-new-user-and-grant-permissions-in-mysql

In MySQL create a new database, e.g. Spike.
```
$mysql> CREATE DATABASE Spike;
```
You can upload our sample data (results from Covid-19 Data Portal from March 2022) to the database our use your own data by running the CovRadar pipeline.

Download sample data:
```
# DOI 10.17605/OSF.IO/7XAJG
wget https://osf.io/qhu6d/download -O Spike.sql
```

Add database Spike:

```
mysql -u USER -p Spike < Spike.sql

# if you get the error: ERROR 1273 (HY000) at line 25: Unknown collation: 'utf8mb4_0900_ai_ci'
sed -i Spike.sql -e 's/utf8mb4_0900_ai_ci/utf8mb4_unicode_ci/g'
```

The credentials and database name come from the system environment variables. Add them into .bashrc.
```
export MYSQL_USER=user_name
export MYSQL_PW=password
export MYSQL_HOST=localhost
export MYSQL_DBglobal=Spike
```
### Start the app locally <a name="app_start_local"></a>
```
python server.py  
```
This will start the app under [http://localhost:8888/report](http://localhost:8888/report).


## Installation - The Docker way <a name="app_installation_docker"></a>

We also provide a containerized environment to run the app. You only need [Docker](https://docs.docker.com/install/linux/docker-ce/ubuntu/) and [docker-compose](https://docs.docker.com/compose/install/) installed. 


### Using Docker compose <a name="app_docker_compose"></a>
Once you have both, 1) clone this repository, 2) download a MySQL data dump calculated with the Snakemake pipeline based on public spike sequences from [EMBL-EBI SARS-CoV-2 sequences](https://www.covid19dataportal.org/) and 3) execute the app locally via `docker-compose`:

```bash
# get the code
git clone https://gitlab.com/dacs-hpi/covradar.git

# get the MySQL data dump (~130 MB)
mkdir frontend/db
wget https://osf.io/qhu6d/download -O frontend/db/Spike.sql

# build the app and database container
docker-compose build

# run the app
docker-compose up
```

The `build` and `up` process will take a couple of minutes. After the successful launch, you can explore your data at [http://0.0.0.0:8888/report/](http://0.0.0.0:8888/report/) on any browser to start.


### Using an existing MySQL database <a name="app_docker_existing_mysql"></a>

Pull the docker container:
```bash
docker pull dacshpi/covradar:latest
```

Run the docker container, providing the MYSQL credentials and database name stored in environment variables:
```bash
docker run -e MYSQL_USER="$MYSQL_USER" -e MYSQL_PW="$MYSQL_PW" -e MYSQL_HOST="$MYSQL_HOST" \
    -e MYSQL_DBglobal="$MYSQL_DBglobal" -p 127.0.0.1:8888:8888 dacshpi/covradar:latest
```


### Use your own data <a name="app_docker_own_data"></a>

If you want to visualize your own data with the app, run the Snakemake pipeline and store the results in a MySQL database like described in the manual. Then, generate a `Spike.sql` dump file and place it in the `covradar/frontend/db` folder. Now, run `docker-compose down && docker-compose build && docker-compose up`.


# Additional Analysis Scripts <a name="analysis_scripts"></a>

## Nucleotide and Amino Acid Translations + Position Conversions <a name="translations_conversions"></a>

CovRadar further offers small functionalities for nucleotide and amino acid translations and position conversions that can be executed without the need to run the pipeline beforehand. The scripts for these tasks are located in the `covradar/shared/residue_converter/` folder. All the subtools are explained in detail in the README.md file within that folder. 

| subcommand       | purpose                                                                                                          | example      |
|------------------|------------------------------------------------------------------------------------------------------------------|--------------|
| aa2nt            | Reverse-translates an AA substitution or deletion pattern into the corresponding possible nt mutation pattern(s) | aa variant: R190S --> G22132T / G22132C / G22132Y |
| nt2aa            | Translates an nt substitution pattern (genomic position) into the corresponding AA substitution pattern          | nt variant: G22132T --> R190S |
| nt_seq2aa_seq    | Translates nucleotide sequence to corresponding amino acid sequence                                              | nt seq: CATTTCCAG --> HFQ |

| subcommand         | purpose                                                                               | example                             |
|--------------------|---------------------------------------------------------------------------------------|-------------------------------------|
| aa2na_gene         | Convert amino acid position to gene position                                          | aa pos: 484 --> (1450, 1452)        |
| aa2na_genome       | Convert amino acid position within gene to whole-genome position                      | aa pos: 484 --> (23012, 23014)      |
| na2aa_gene         | Convert nucleotide position within gene to amino acid position within gene            | na pos: 1450 --> 484                |
| na2aa_genome       | Convert whole-genome nucleotide position to amino acid position within gene           | na pos: 23012 --> 484               |
| na_gene_to_genome  | Convert gene position to whole-genome position                                        | na pos: 1450 --> 23012              |
| na_genome_to_gene  | Convert whole-genome position to gene position                                        | na pos: 23012 --> 1450              |

## Visualization of the Mutations of Concern Local Distribution in the Map (using the app)
To visualize the mutations per location in the map and in location specific plots, the following must be done: 
 * add the column location_ID to the global_metadata table 
 * add a location_coordinates table to the database containing the location_ID, name and the geographical coordinates of the longitude and latitude. 

By assigning the sequences to different locations, the spatial resolution can be adjusted as desired.

Database schema table location_coordinates:

Fields (type): location_ID (int NOT NULL, primary key), name (varchar), lat (decimal(15,10)), lon (decimal(15,10))

## Visualization of Nucleotide and Amino Acid Frequencies (without using the app) <a name="vizualize_without_app"></a>

For the demand to visualize and investigate nucleotide and amino acid frequencies for specific positions and regions generated by the pipeline without using the app CovRadar provides scripts in the `covradar/analysis/` folder. How to use these scripts is again explained in the README.md file within the folder.

## Gene Extraction <a name="gene_extraction"></a>

To perform a pipeline-independent extraction of gene sequences from a whole-genome Multi-FASTA file, the script `gene_extraction.py` within the folder `shared/gene_extraction/` can be used. The detailed workflow and usage is explained in the local README.md file.