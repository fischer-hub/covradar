# Database update in Kubernetes

## Requirements
- s3cmd (https://s3tools.org/s3cmd)
- kubectl (https://kubernetes.io/docs/reference/kubectl/overview/)

## Steps
For each datbase the following steps have to be performed:

1. Upload to s3
    * ```s3cmd put <localfile> s3://<bucket>/data/<filename>```
2. Create Presigned link
    * ```s3cmd signurl s3://<bucket>/data/<filename> +3600```
3. Copy link into loader-job.yaml depending on which datanase should be used
4. Change SQL_HOST to standby database
5. Execute update Job -> wait until finish
    * ```kubectl apply -f <loader-file> -n <namespace>```
6. Change active service in Rancher:
    * Covradar-Namespace -> Service Discovery -> covradar-db service -> edit -> change to updated database
7. Delete Job
    * kubectl delete -f <loader-file> -n <namespace>