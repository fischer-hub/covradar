# ************************************************************
# Sequel Ace SQL dump
# Version 3041
#
# https://sequel-ace.com/
# https://github.com/Sequel-Ace/Sequel-Ace
#
# Host: localhost (MySQL 8.0.23)
# Datenbank: test_sql_load
# Verarbeitungszeit: 2021-10-17 11:05:56 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
SET NAMES utf8mb4;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE='NO_AUTO_VALUE_ON_ZERO', SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Tabellen-Dump global_metadata
# ------------------------------------------------------------

DROP TABLE IF EXISTS `global_metadata`;

CREATE TABLE `global_metadata` (
  `strain` varchar(250) NOT NULL,
  `virus` varchar(64) DEFAULT NULL,
  `accession` varchar(64) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `region` varchar(64) DEFAULT NULL,
  `country` varchar(64) DEFAULT NULL,
  `division` varchar(64) DEFAULT NULL,
  `country_exposure` varchar(64) DEFAULT NULL,
  `originating_lab` varchar(500) DEFAULT NULL,
  `submitting_lab` varchar(500) DEFAULT NULL,
  `authors` varchar(255) DEFAULT NULL,
  `origin` varchar(64) DEFAULT NULL,
  `host` varchar(64) DEFAULT NULL,
  `strain_id` int unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`strain_id`),
  KEY `global_metadata_strain_IDX` (`strain`) USING BTREE,
  KEY `global_metadata_strain_id_IDX` (`strain_id`) USING BTREE,
  KEY `global_metadata_date_IDX` (`date`) USING BTREE,
  KEY `global_metadata_host_IDX` (`host`) USING BTREE,
  KEY `global_metadata_country_IDX` (`country`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

LOCK TABLES `global_metadata` WRITE;
/*!40000 ALTER TABLE `global_metadata` DISABLE KEYS */;

INSERT INTO `global_metadata` (`strain`, `virus`, `accession`, `date`, `region`, `country`, `division`, `country_exposure`, `originating_lab`, `submitting_lab`, `authors`, `origin`, `host`, `strain_id`)
VALUES
	(X'61',NULL,NULL,'2020-03-16',NULL,X'4765726D616E79',NULL,NULL,NULL,NULL,NULL,X'676973616964',X'68756D616E',1),
	(X'62',NULL,NULL,'2020-03-20',NULL,X'4765726D616E79',NULL,NULL,NULL,NULL,NULL,X'636861726974C3A9',X'6D696E6B',2),
	(X'63',NULL,NULL,'2020-03-20',NULL,X'43616E616461',NULL,NULL,NULL,NULL,NULL,X'676973616964',X'68756D616E',3),
	(X'64',NULL,NULL,'2020-04-13',NULL,X'43616E616461',NULL,NULL,NULL,NULL,NULL,X'656269',X'6D696E6B',4),
	(X'65',NULL,NULL,'2020-04-13',NULL,X'4E6F72776179',NULL,NULL,NULL,NULL,NULL,X'636861726974C3A9',X'68756D616E',5),
	(X'66',NULL,NULL,'2020-07-12',NULL,X'4E6F72776179',NULL,NULL,NULL,NULL,NULL,X'656269',X'6D696E6B',6),
	(X'67',NULL,NULL,'2020-07-12',NULL,X'57616C6573',NULL,NULL,NULL,NULL,NULL,X'656269',X'68756D616E',7);

/*!40000 ALTER TABLE `global_metadata` ENABLE KEYS */;
UNLOCK TABLES;


# Tabellen-Dump global_SNP
# ------------------------------------------------------------

DROP TABLE IF EXISTS `global_SNP`;

CREATE TABLE `global_SNP` (
  `strain_id` int unsigned NOT NULL,
  `position` int NOT NULL,
  `REF_allele` varchar(2) DEFAULT NULL,
  `ALT` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`strain_id`,`position`),
  CONSTRAINT `fk_global_metadata_global_SNP` FOREIGN KEY (`strain_id`) REFERENCES `global_metadata` (`strain_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

LOCK TABLES `global_SNP` WRITE;
/*!40000 ALTER TABLE `global_SNP` DISABLE KEYS */;

INSERT INTO `global_SNP` (`strain_id`, `position`, `REF_allele`, `ALT`)
VALUES
	(1,2,X'41',X'54'),
	(1,7,X'41',X'54'),
	(2,3,X'41',X'43'),
	(2,10,X'41',X'54'),
	(3,2,X'41',X'54'),
	(3,10,X'41',X'54'),
	(4,2,X'41',X'47'),
	(4,10,X'41',X'47'),
	(5,4,X'41',X'54'),
	(6,4,X'41',X'43');

/*!40000 ALTER TABLE `global_SNP` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
