-- MySQL dump 10.13  Distrib 5.7.31, for Linux (x86_64)
--
-- Host: localhost    Database: test_consensus
-- ------------------------------------------------------
-- Server version	5.7.31-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `base_count`
--

DROP TABLE IF EXISTS `base_count`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_count` (
  `region` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL,
  `position` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  PRIMARY KEY (`region`,`date`,`position`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `base_count`
--

LOCK TABLES `base_count` WRITE;
/*!40000 ALTER TABLE `base_count` DISABLE KEYS */;
INSERT INTO `base_count` VALUES ('Germany','2019W40',1,100),('Germany','2019W40',2,99),('Germany','2019W40',3,98),('Germany','2019W40',4,97),('Germany','2019W40',5,96),('Germany','2019W40',6,95),('Germany','2020W10',1,94),('Germany','2020W10',2,93),('Germany','2020W10',3,92),('Germany','2020W10',4,91),('Germany','2020W10',5,90),('Germany','2020W10',6,89),('Germany','2020W40',1,88),('Germany','2020W40',2,87),('Germany','2020W40',3,86),('Germany','2020W40',4,85),('Germany','2020W40',5,84),('Germany','2020W40',6,83),('Norway','2019W40',1,190),('Norway','2019W40',2,189),('Norway','2019W40',3,188),('Norway','2019W40',4,187),('Norway','2019W40',5,186),('Norway','2019W40',6,185),('Norway','2020W10',1,184),('Norway','2020W10',2,183),('Norway','2020W10',3,182),('Norway','2020W10',4,181),('Norway','2020W10',5,180),('Norway','2020W10',6,179),('Norway','2020W40',1,178),('Norway','2020W40',2,177),('Norway','2020W40',3,176),('Norway','2020W40',4,175),('Norway','2020W40',5,174),('Norway','2020W40',6,173);
/*!40000 ALTER TABLE `base_count` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `consensus`
--

DROP TABLE IF EXISTS `consensus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `consensus` (
  `region` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL,
  `consensus` text NOT NULL,
  `number_of_sequences` int(11) DEFAULT NULL,
  PRIMARY KEY (`region`,`date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `consensus`
--

LOCK TABLES `consensus` WRITE;
/*!40000 ALTER TABLE `consensus` DISABLE KEYS */;
INSERT INTO `consensus` VALUES ('Germany','2019W40','ATATAT',100),('Germany','2020W10','ATATAG',110),('Germany','2020W40','AAATAG',120),('Norway','2019W40','TATATA',190),('Norway','2020W10','GATATA',200),('Norway','2020W40','GTAATA',210),('Wuhan','first-case','AAAAAA',1);
/*!40000 ALTER TABLE `consensus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `global_position`
--

DROP TABLE IF EXISTS `global_position`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `global_position` (
  `first_case` varchar(255) NOT NULL,
  `msa` int(11) NOT NULL,
  PRIMARY KEY (`msa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `global_position`
--

LOCK TABLES `global_position` WRITE;
/*!40000 ALTER TABLE `global_position` DISABLE KEYS */;
INSERT INTO `global_position` VALUES ('1',1),('1.1',2),('1.2',3),('2',4),('3',5),('4',6);
/*!40000 ALTER TABLE `global_position` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-11-17 15:47:34
