# ************************************************************
# Sequel Ace SQL dump
# Version 3041
#
# https://sequel-ace.com/
# https://github.com/Sequel-Ace/Sequel-Ace
#
# Host: localhost (MySQL 8.0.23)
# Datenbank: test_freq
# Verarbeitungszeit: 2021-10-17 11:13:59 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
SET NAMES utf8mb4;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE='NO_AUTO_VALUE_ON_ZERO', SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Tabellen-Dump consensus
# ------------------------------------------------------------

DROP TABLE IF EXISTS `consensus`;

CREATE TABLE `consensus` (
  `region` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL,
  `consensus` text NOT NULL,
  PRIMARY KEY (`region`,`date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `consensus` WRITE;
/*!40000 ALTER TABLE `consensus` DISABLE KEYS */;

INSERT INTO `consensus` (`region`, `date`, `consensus`)
VALUES
	('x','2020W36','AACTCA-AAGATGAATCAATCAGATA'),
	('y','first-case','AATACA-AAGATGAATCAATCAGATA');

/*!40000 ALTER TABLE `consensus` ENABLE KEYS */;
UNLOCK TABLES;


# Tabellen-Dump global_metadata
# ------------------------------------------------------------

DROP TABLE IF EXISTS `global_metadata`;

CREATE TABLE `global_metadata` (
  `strain` varchar(250) NOT NULL,
  `virus` varchar(64) DEFAULT NULL,
  `accession` varchar(64) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `region` varchar(64) DEFAULT NULL,
  `country` varchar(64) DEFAULT NULL,
  `division` varchar(64) DEFAULT NULL,
  `country_exposure` varchar(64) DEFAULT NULL,
  `originating_lab` varchar(500) DEFAULT NULL,
  `submitting_lab` varchar(500) DEFAULT NULL,
  `authors` varchar(255) DEFAULT NULL,
  `origin` varchar(64) DEFAULT NULL,
  `host` varchar(64) DEFAULT NULL,
  `strain_id` int unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`strain_id`),
  KEY `global_metadata_strain_IDX` (`strain`) USING BTREE,
  KEY `global_metadata_strain_id_IDX` (`strain_id`) USING BTREE,
  KEY `global_metadata_date_IDX` (`date`) USING BTREE,
  KEY `global_metadata_host_IDX` (`host`) USING BTREE,
  KEY `global_metadata_country_IDX` (`country`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

LOCK TABLES `global_metadata` WRITE;
/*!40000 ALTER TABLE `global_metadata` DISABLE KEYS */;

INSERT INTO `global_metadata` (`strain`, `virus`, `accession`, `date`, `region`, `country`, `division`, `country_exposure`, `originating_lab`, `submitting_lab`, `authors`, `origin`, `host`, `strain_id`)
VALUES
	(X'73657131',NULL,NULL,'2020-11-03',NULL,X'78',NULL,NULL,NULL,NULL,NULL,X'656269',X'68756D616E',1),
	(X'73657132',NULL,NULL,'2020-11-03',NULL,X'78',NULL,NULL,NULL,NULL,NULL,X'656269',X'68756D616E',2),
	(X'66697273742D63617365',NULL,NULL,'2020-11-03',NULL,X'79',NULL,NULL,NULL,NULL,NULL,X'656269',X'68756D616E',3);

/*!40000 ALTER TABLE `global_metadata` ENABLE KEYS */;
UNLOCK TABLES;


# Tabellen-Dump global_position
# ------------------------------------------------------------

DROP TABLE IF EXISTS `global_position`;

CREATE TABLE `global_position` (
  `first_case` varchar(255) NOT NULL,
  `msa` int NOT NULL,
  PRIMARY KEY (`msa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Tabellen-Dump global_SNP
# ------------------------------------------------------------

DROP TABLE IF EXISTS `global_SNP`;

CREATE TABLE `global_SNP` (
  `strain_id` int unsigned NOT NULL,
  `position` int NOT NULL,
  `REF_allele` varchar(2) DEFAULT NULL,
  `ALT` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`strain_id`,`position`),
  CONSTRAINT `fk_global_metadata_global_SNP` FOREIGN KEY (`strain_id`) REFERENCES `global_metadata` (`strain_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

LOCK TABLES `global_SNP` WRITE;
/*!40000 ALTER TABLE `global_SNP` DISABLE KEYS */;

INSERT INTO `global_SNP` (`strain_id`, `position`, `REF_allele`, `ALT`)
VALUES
	(1,2,X'41',X'54'),
	(1,3,X'54',X'41'),
	(1,6,X'41',X'57'),
	(1,7,X'2D',X'4E'),
	(1,8,X'41',X'4E'),
	(1,9,X'41',X'53'),
	(1,11,X'41',X'4D'),
	(1,13,X'47',X'4B'),
	(1,15,X'41',X'52'),
	(1,17,X'43',X'59'),
	(1,19,X'41',X'42'),
	(1,21,X'43',X'44'),
	(1,23,X'47',X'48'),
	(1,25,X'54',X'56'),
	(2,1,X'41',X'54'),
	(2,2,X'41',X'47'),
	(2,3,X'54',X'43'),
	(2,5,X'43',X'57'),
	(2,8,X'41',X'4E'),
	(2,10,X'47',X'53'),
	(2,12,X'54',X'4D'),
	(2,14,X'41',X'4B'),
	(2,16,X'54',X'52'),
	(2,18,X'41',X'59'),
	(2,20,X'54',X'42'),
	(2,22,X'41',X'44'),
	(2,24,X'41',X'48'),
	(2,26,X'41',X'56');

/*!40000 ALTER TABLE `global_SNP` ENABLE KEYS */;
UNLOCK TABLES;


# Tabellen-Dump global_statistics
# ------------------------------------------------------------

DROP TABLE IF EXISTS `global_statistics`;

CREATE TABLE `global_statistics` (
  `date` varchar(10) NOT NULL,
  `taxa` int NOT NULL,
  `sites` int NOT NULL,
  `undetermined` int NOT NULL,
  `duplicates` int NOT NULL,
  `partitions` int NOT NULL,
  `patterns` int NOT NULL,
  `gaps` double NOT NULL,
  `invariants` double NOT NULL,
  `snps` int NOT NULL,
  `multiallelic_sites` int NOT NULL,
  `multiallelic_snp_sites` int NOT NULL,
  PRIMARY KEY (`date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
