-- MySQL dump 10.13  Distrib 8.0.25, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: TestDBworldMap
-- ------------------------------------------------------
-- Server version	5.5.5-10.6.5-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `global_ID`
--

DROP TABLE IF EXISTS `global_ID`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `global_ID` (
                             `strain_id` int(11) NOT NULL AUTO_INCREMENT,
                             `strain` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                             PRIMARY KEY (`strain_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `global_ID`
--

LOCK TABLES `global_ID` WRITE;
/*!40000 ALTER TABLE `global_ID` DISABLE KEYS */;
INSERT INTO `global_ID` VALUES (1,'seq1');
/*!40000 ALTER TABLE `global_ID` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `global_metadata`
--

DROP TABLE IF EXISTS `global_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `global_metadata` (
                                   `strain_id` int(3) NOT NULL,
                                   `location_ID` int(6) DEFAULT NULL,
                                   `date` date DEFAULT NULL,
                                   `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                   PRIMARY KEY (`strain_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `global_metadata`
--

LOCK TABLES `global_metadata` WRITE;
/*!40000 ALTER TABLE `global_metadata` DISABLE KEYS */;
INSERT INTO `global_metadata` VALUES (1,10115,'2022-01-01','Berlin'),(2,10115,'2022-01-01','Berlin'),(3,10115,'2022-01-01','Berlin'),(4,10115,'2022-01-01','Berlin'),(5,10115,'2022-01-01','Berlin'),(6,80331,'2022-01-01','München'),(7,80331,'2022-01-01','München'),(8,10115,'2022-01-10','Berlin'),(9,30161,'2022-01-10','Hannover'),(10,30161,'2022-01-10','Hannover'),(11,30161,'2022-01-10','Hannover'),(12,30161,'2022-01-10','Hannover'),(13,80331,'2022-01-10','München'),(14,80331,'2022-01-10','München'),(15,80331,'2022-01-10','München'),(16,1,'2022-01-07','France'),(17,1,'2022-01-07','France'),(18,1,'2022-01-07','France'),(19,1,'2022-01-07','France'),(20,2,'2022-01-07','Austria'),(21,2,'2022-01-07','Austria');
/*!40000 ALTER TABLE `global_metadata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `global_position`
--

DROP TABLE IF EXISTS `global_position`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `global_position` (
                                   `first_case` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                   `msa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `global_position`
--

LOCK TABLES `global_position` WRITE;
/*!40000 ALTER TABLE `global_position` DISABLE KEYS */;
INSERT INTO `global_position` VALUES ('1',1),('2',2),('3',3),('4',4),('5',5);
/*!40000 ALTER TABLE `global_position` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `location_coordinates`
--

DROP TABLE IF EXISTS `location_coordinates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `location_coordinates` (
                                        `location_ID` int(6) NOT NULL,
                                        `country_ID` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                        `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                        `lon` decimal(15,10) DEFAULT NULL,
                                        `lat` decimal(15,10) DEFAULT NULL,
                                        PRIMARY KEY (`location_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `location_coordinates`
--

LOCK TABLES `location_coordinates` WRITE;
/*!40000 ALTER TABLE `location_coordinates` DISABLE KEYS */;
INSERT INTO `location_coordinates` VALUES (1,'FR','France',2.2137490000,46.2276380000),(2,'AT','Austria',14.5500720000,47.5162310000),(10115,'DE','Berlin',13.3872000000,32.5337000000),(30161,'DE','Hannover',9.7446000000,52.3842000000),(80331,'DE','München',11.5722000000,48.1379000000);
/*!40000 ALTER TABLE `location_coordinates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sequence_voc`
--

DROP TABLE IF EXISTS `sequence_voc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sequence_voc` (
                                `strain_id` int(3) DEFAULT NULL,
                                `voc_id` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sequence_voc`
--

LOCK TABLES `sequence_voc` WRITE;
/*!40000 ALTER TABLE `sequence_voc` DISABLE KEYS */;
INSERT INTO `sequence_voc` VALUES (1,39),(4,39),(5,39),(11,39),(12,39),(15,39),(17,39),(18,39),(19,39),(21,39),(4,40),(5,40),(7,40),(11,40),(12,40),(13,40),(14,40),(18,40),(21,40),(4,42),(5,42),(6,42),(13,42),(14,42),(17,42),(19,42),(21,42),(4,45),(5,45),(13,45),(14,45),(15,45),(21,45),(4,46),(5,46),(6,46),(17,46),(19,46),(21,46);
/*!40000 ALTER TABLE `sequence_voc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `variants_of_concern`
--

DROP TABLE IF EXISTS `variants_of_concern`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `variants_of_concern` (
                                       `nucleotide` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                       `amino_acid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                       `msa` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                                       `id` int(11) NOT NULL AUTO_INCREMENT,
                                       PRIMARY KEY (`id`),
                                       UNIQUE KEY `nucleotide` (`nucleotide`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `variants_of_concern`
--

LOCK TABLES `variants_of_concern` WRITE;
/*!40000 ALTER TABLE `variants_of_concern` DISABLE KEYS */;
INSERT INTO `variants_of_concern` VALUES ('C21614T','L18F','94\n',39),('C21621A','T20N','110\n',40),('del:21765:6','A475V','401:406\n',42),('del:21992:3','D138Y','937:939\n',45),('G22132C/G22132T','E484K','1326\n',46);
/*!40000 ALTER TABLE `variants_of_concern` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-03-01 15:41:18
