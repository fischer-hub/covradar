import unittest
import numpy as np

from frontend.app.shared.data import load_all_sql_files
from frontend.app.shared.utils_base_freq_plotter import basefreqplotter


class TestFreqPlot(unittest.TestCase):
    database_name = "test_freq_plot"

    def setUp(self):
        # data base import
        self.df_dict = load_all_sql_files(self.database_name)

    def test_freq_plot(self):
        # tests if allele frequency plot is in right order
        freq_list = np.array(1000 * [1.] + 1000 * [0.5] + 1000 * [0.1])
        base_cov = np.zeros(3000)
        variants_db = ['A475V', 'E484K,N501Y', 'Y144-144del']
        variants_usr = '215;812-815'
        domains = ['RBD']
        fig = basefreqplotter(self.df_dict['global_position'], self.df_dict["first_msa"],
                              freq_list, base_cov, variants_db, variants_usr, domains)
        self.assertEqual(fig["data"][0]["z"][-1][0], 1.0)
        self.assertEqual(fig["data"][0]["z"][0][0], 0.1)
        self.assertEqual(fig["data"][0]["z"][0][-1], None)
        # TODO: test tooltip /base coverage
