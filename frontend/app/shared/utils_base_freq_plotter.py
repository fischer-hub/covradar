import math
import matplotlib
from matplotlib import cm
import numpy as np
import plotly.graph_objects as go
import re

from shared.residue_converter.position_converter import na2aa_msa_gene, aa2na_gene


def get_domain_span(global_position, domain):
    """
    Get the domain span (start, end) from gene position to MSA position.
    Args:
        df_dict:
        domain: str, domain name

    Returns:
    loc: (data frame), 2x2 with first_case and msa position as columns

    """
    # TODO replace with database query for domain and protein
    domains = {k: (i, j) for i, j, k in
               zip([13, 319, 788, 912, 1163, 1213, 1237], [305, 541, 806, 984, 1213, 1337, 1273],
                   ["NTD", "RBD", "FP", "HR1", "HR2", "TM", "CT"])}
    # get start and end of domain
    # from AA positiont to genome
    start = aa2na_gene(domains[domain][0])[0]
    end = aa2na_gene(domains[domain][1])[1]
    # example for RBD
    # 988 → 1749 RBD domain of first case (NC_045512)
    return global_position[global_position['first_case'].isin([str(start),str(end)])].reset_index(drop=True)


def chunks(lst, n):
    """
    # for visualization I'm converting the 1D array to 2D
    Takes 1D list lst and number n and splits list after n elements
    Fills last segment with -1 if its length is not n
    output: 2D list
    """
    result = []
    for i in range(0, len(lst), n):
        chunk = lst[i:i + n]
        result.append(list(chunk))
        if len(chunk) < n:
            for i in range(n - len(chunk)):
                result[-1].append(None)
    return result


def filter_for_variants_usr(variants_usr, pos_table):
    # remove spaces
    variants_user = variants_usr.replace(" ", "")
    # split the codons (separated by ";")
    variants_user = np.unique(variants_user.split(";"))
    # and split the spans (contain -)
    variants_user = [i.split("-") for i in variants_user]
    variants_user = [
        # distinguish spans, here we need the start of the 1st codon and the end of the 2nd
        (f"{i[0]}-{i[1]} (user)", [aa2na_gene(int(i[0]))[0], aa2na_gene(int(i[1]))[0] + 2],
         "variant") if len(i) == 2 else
        # and single codons, here the aa2na will return the right range
        (f"{i[0]} (user)", [*aa2na_gene(int(i[0]))], "variant") for i in variants_user]
    # convert positions of custom codons from spike-based to msa-based
    for entry in variants_user:
        # start pos
        start = entry[1][0]
        start_index = pos_table.index[pos_table["first_case"] == str(start)].tolist()[0]
        start_msa = pos_table.at[start_index, "msa"]
        entry[1][0] = start_msa
        # stop pos
        stop = entry[1][1]
        stop_index = pos_table.index[pos_table["first_case"] == str(stop)].tolist()[0]
        stop_msa = pos_table.at[stop_index, "msa"]
        entry[1][1] = stop_msa
    return variants_user


def filter_variants_db(variants_db, first_msa):
    # pattern to get position(s) of the mutation
    pat = re.compile(r"(\d+)")
    # we need to resolve ambig. mutations in the same location
    # aggregate these mutations
    variant_dic = {}
    # print('variants_db:', variants_db)
    for variant_voc in np.unique(variants_db):
        # returns multiple numbers for deletions / inserts
        splitted_voc = variant_voc.split(',')  # separate double mutations if present (<class 'list'>)
        for voc in splitted_voc:
            pos = "-".join(re.findall(pat, voc))
            # handle duplicated positions in dictionary
            if pos in variant_dic:
                if voc not in variant_dic[pos]:
                    variant_dic[pos].append(voc)
            else:
                variant_dic[pos] = [voc]

    # iterate over dictionary and make sure pivot the elements in the dicationary again
    variants_db = \
        sorted([(";".join(variant_dic[mut]),
                 np.apply_along_axis(aa2na_gene, 0, np.array(mut.split("-")).astype(int))[:, 0],
                 "variant")
                # the above variant is used when there are SNPs
                if "-" not in mut else
                # this is necessary when spans are treated (deletions)
                (";".join(variant_dic[mut]),
                 np.apply_along_axis(aa2na_gene, 0,
                                     np.array(mut.split("-")).astype(int)).diagonal(),
                 "variant") for mut in variant_dic.keys()])

    # Spike position to MSA position
    variants_db = [(el[0], [first_msa[str(pos)] for pos in el[1]], el[2]) for el in variants_db]
    return variants_db


def create_voc_lookup(voc_ar, spike_length):
    voc_lookup = {}
    voc_lookup_placeholders = {}
    for voc in voc_ar:
        pos_range = list(range(voc[1][0] - 1, voc[1][1]))
        name = voc[0]
        for pos in pos_range:
            if pos in voc_lookup:
                if name not in voc_lookup[pos]:
                    voc_lookup[pos] = voc_lookup[pos] + ';' + name
                    voc_lookup_placeholders[pos] = 'Position of MOC: '
            else:
                voc_lookup[pos] = name
                voc_lookup_placeholders[pos] = 'Position of MOC: '
    for i in range(0, spike_length + 1):
        if i not in voc_lookup:
            voc_lookup[i] = ""
            voc_lookup_placeholders[i] = ""
    return voc_lookup, voc_lookup_placeholders


def create_color_scale():
    tab20c_r_cmap = matplotlib.cm.get_cmap('tab20c_r')
    tab20c_r = []
    norm = matplotlib.colors.Normalize(vmin=0, vmax=255)

    for i in range(0, 256):
        if i == 0:
            # zero to 1/300 get different color
            tab20c_r.append([norm(i), "rgb(250, 250, 245)"])
        else:
            k = matplotlib.colors.colorConverter.to_rgb(tab20c_r_cmap(norm(i)))
            tab20c_r.append([norm(i), "rgb" + str(k)])
    return tab20c_r


def create_nested_array(freq_list, row_len):
    # converted 2D array
    nested_array = chunks(freq_list, row_len)[::-1]
    # format .4f or None
    nested_array = [[np.round(v, 4) if isinstance(v, float) else v for v in i] for i in
                nested_array]
    return nested_array


def create_pos_label(spike_length, start_pos, len_nested_array, row_len, first_case, base_cov, voc_lookup,
                     voc_lookup_placeholders):
    msa_labels = np.array([["" if i >= spike_length else str(i + start_pos) for i in \
                            np.arange(len_nested_array * row_len)]]).reshape(len_nested_array,
                                                                             row_len)[::-1]

    spike_labels = np.array([["" if i >= spike_length else first_case[i] \
                              for i in np.arange(len_nested_array * row_len)]]).reshape(
        len_nested_array, row_len)[::-1]

    aa_labels = np.array([["" if i >= spike_length else na2aa_msa_gene(first_case[i]) \
                           for i in np.arange(len_nested_array * row_len)]]).reshape(
        len_nested_array, row_len)[::-1]

    coverage_labels = np.array([["" if i >= spike_length else str(base_cov[i]) \
                                 for i in np.arange(len_nested_array * row_len)]]).reshape(
        len_nested_array, row_len)[::-1]

    voc_labels = np.array([["" if i >= spike_length else voc_lookup[i] \
                            for i in np.arange(len_nested_array * row_len)]]).reshape(
        len_nested_array, row_len)[::-1]

    voc_placeholders = np.array([["" if i >= spike_length else voc_lookup_placeholders[i] \
                                  for i in np.arange(len_nested_array * row_len)]]).reshape(
        len_nested_array, row_len)[::-1]
    position_label = np.dstack((msa_labels, spike_labels, aa_labels, coverage_labels, voc_labels, voc_placeholders))
    return position_label


def create_fig(nested_array, tab20c_r, position_label, len_y_axis_labels, y_axis_labels, row_len, annotation_ar, no_rows):
    fig = go.Figure(data=go.Heatmap(z=nested_array,
                                    xgap=1.2, ygap=1.2,
                                    colorscale=tab20c_r,
                                    zmin=0.00001, zmax=1.,
                                    customdata=position_label,
                                    hovertemplate=
                                    'MSA: %{customdata[0]} <br>' +
                                    'Spike: %{customdata[1]} <br>' +
                                    'Codon: %{customdata[2]} <br>' +
                                    '%{customdata[5]}%{customdata[4]}' +
                                    '<extra>Frequency: %{z} <br>' +
                                    'Coverage: %{customdata[3]}' +
                                    '</extra>'))

    x_axis_template = dict(showticklabels=False,
                           tickmode="linear",
                           tick0=0,
                           dtick=1,
                           showspikes=True)

    y_axis_template = dict(tickmode='array',
                           tickvals=list(range(len_y_axis_labels)),
                           ticktext=list(reversed(y_axis_labels)),
                           tickfont={"size": 10},
                           showspikes=True,
                           scaleanchor="x",
                           scaleratio=1,
                           )

    fig.update_layout(xaxis=x_axis_template,
                      yaxis=y_axis_template,
                      autosize=True,
                      plot_bgcolor='rgba(0,0,0,0)',  # no background color
                      margin=dict(
                          l=0,
                          r=0,
                          b=0,
                          t=25,
                          pad=0)
                      )

    # domain boxes
    box_annotations = []
    variants_annots = []
    # print('annotation_ar:', annotation_ar)
    # print('-----------------------')
    for ind, dom in enumerate(annotation_ar):
        start_dom = dom[1][0]
        end_dom = dom[1][1] + 1

        # print('dom:', dom)

        # row specifies the height, where to plot the annotation
        row = np.floor(no_rows - (start_dom / row_len))

        res_start = list(filter(lambda i: i <= start_dom, y_axis_labels[::-1]))[0]
        # print('res_start:', res_start)
        y_box_start = y_axis_labels.index(res_start)
        x_box_start = start_dom - res_start
        # print('y_box_start:', y_box_start)
        # print('x_box_start:', x_box_start)

        res_end = list(filter(lambda i: i < end_dom, y_axis_labels[::-1]))[0]
        # print('res_end:', res_end)
        y_box_end = y_axis_labels.index(res_end)
        # print('y_box_end:', y_box_end)
        x_box_end = end_dom - res_end
        # print('x_box_end:', x_box_end)

        # domain is in same row
        if y_box_start == y_box_end:
            nodes = [(x_box_start - 0.5, len_y_axis_labels - 1 - y_box_start + 0.5),  # upper left corner
                     (x_box_end - 0.5, len_y_axis_labels - 1 - y_box_end + 0.5),  # upper right corner
                     (x_box_end - 0.5, len_y_axis_labels - 1 - y_box_end - 1 + 0.5),  # lower right corner
                     (x_box_start - 0.5, len_y_axis_labels - 1 - y_box_end - 1 + 0.5)]  # lower left corner
        # domain spans over two rows but is shorter than length of row + 1
        elif y_box_start == y_box_end - 1 and x_box_end < x_box_start:
            nodes = [(row_len - 0.5, len_y_axis_labels - 1 - y_box_start - 1 + 0.5),
                     # start point (upper right corner of box in 1st row)
                     (x_box_start - 0.5, len_y_axis_labels - 1 - y_box_start - 1 + 0.5),
                     # go left until start of the box
                     (x_box_start - 0.5, len_y_axis_labels - 1 - y_box_start + 0.5),  # go down one row
                     (row_len - 0.5, len_y_axis_labels - 1 - y_box_start + 0.5),  # go right until the end of the row
                     'sep',  # to set new start point
                     (0 - 0.5, len_y_axis_labels - 1 - y_box_end - 1 + 0.5),
                     # start point (lower left corner of box in 2nd row)
                     (x_box_end - 0.5, len_y_axis_labels - 1 - y_box_end - 1 + 0.5),  # go right until end of the box
                     (x_box_end - 0.5, len_y_axis_labels - 1 - y_box_end + 0.5),  # go up one row
                     (0 - 0.5, len_y_axis_labels - 1 - y_box_end + 0.5)]  # go left until start of the row
        else:
            nodes = [(x_box_start - 0.5, len_y_axis_labels - 1 - y_box_start + 0.5),  # start point
                     (row_len - 0.5, len_y_axis_labels - 1 - y_box_start + 0.5),  # go right until end of row
                     (row_len - 0.5, len_y_axis_labels - 1 - y_box_end + 0.5),
                     # go down until second lowest row of box (y)
                     (x_box_end - 0.5, len_y_axis_labels - 1 - y_box_end + 0.5),  # go left until end position of x row
                     (x_box_end - 0.5, len_y_axis_labels - 1 - y_box_end - 1 + 0.5),  # go down until last row of box
                     (0 - 0.5, len_y_axis_labels - 1 - y_box_end - 1 + 0.5),  # go left until start of row
                     (0 - 0.5, len_y_axis_labels - 1 - y_box_start - 1 + 0.5),  # go up until second highest row of box
                     (x_box_start - 0.5,
                      len_y_axis_labels - 1 - y_box_start - 1 + 0.5)]  # go right until start of box (x start position)
        # print('nodes:', nodes)

        if y_box_start == y_box_end - 1 and x_box_end < x_box_start:
            domain_box_svg_path = str(nodes).replace(" ", "").replace("(", "L").replace("[L", " M "). \
                replace("),", ", ").replace(")]", "").replace(", ", " ").replace("'sep',L", "M ")
        else:
            domain_box_svg_path = str(nodes).replace(" ", "").replace("(", "L").replace("[L", " M "). \
                replace("),", ", ").replace(")]", " Z").replace(", ", " ")

        # print('domain_box_svg_path:', domain_box_svg_path)

        if dom[2] == "domain":
            # text annotation for domains, right to the heatmap
            annotation = [
                dict(x=row_len + 2,
                     y=row, xref="x", yref="y", text=dom[0], showarrow=False,
                     font=dict(size=10))]

            # box to draw
            box = [dict(type="path", path=domain_box_svg_path, line=dict(color="LightSeaGreen"))]

            # method to be passed to the button below
            box_annotations.append(dict(label=dom[0], method="relayout",
                                        args=[{"shapes": box, "annotations": annotation}]))
        else:
            annotation = [  # -3
                dict(x=(x_box_start + x_box_end) / 2. - 0.5,
                     y=row + 1, xref="x", yref="y", text="", showarrow=False,
                     font=dict(size=10))]

            box = [dict(type="path", path=domain_box_svg_path)]
            variants_annots.append(dict(label=dom[0], method="relayout",
                                        args=[{"shapes": box, "annotations": annotation}]))

        # print('---------------------------')

    # draw all domains per default
    ann_dict = {"shapes": [], "annotations": []}
    box_ann = {"shapes": [i["args"][0]["shapes"][0] for i in box_annotations],
               "annotations": [i["args"][0]["annotations"][0] for i in box_annotations]}
    var_ann = {"shapes": [j["args"][0]["shapes"][0] for j in variants_annots],
               "annotations": [j["args"][0]["annotations"][0] for j in variants_annots]}
    ann_dict["shapes"] = [*box_ann["shapes"], *var_ann["shapes"]]
    ann_dict["annotations"] = [*box_ann["annotations"], *var_ann["annotations"]]

    fig.update_layout(ann_dict)

    # create the button
    # domains
    # fig.update_layout(
    #     updatemenus=[
    #         # domains
    #         dict(buttons=list([
    #             dict(label="All", method="relayout",
    #                  args=[{"shapes": [i["args"][0]["shapes"][0] for i in box_annotations],
    #                         "annotations": [i["args"][0]["annotations"][0] for i in
    #                                         box_annotations]}]),
    #             dict(label="None",
    #                  method="relayout",
    #                  args=[{"shapes": [], "annotations": []}]),
    #             # individual domains
    #             *box_annotations,
    #             # all domains
    #         ]), direction="down", xanchor="left", x=0.0, y=1.08, yanchor="top")]
    # variants
    # dict(buttons=list([
    #     dict(label="All", method="relayout",
    #          args=[{"shapes": [i["args"][0]["shapes"][0] for i in variants_annots],
    #                 "annotations": [i["args"][0]["annotations"][0] for i in
    #                                 variants_annots]}]),
    #     dict(label="None",
    #          method="relayout",
    #          args=[{"shapes": [], "annotations": []}]),
    #     # individual variants
    #     *variants_annots,
    #     # all variants
    #
    # ]), direction="down", xanchor="left", x=0.2, y=1.08, yanchor="top")]
    # )
    # Update remaining layout properties
    # fig.update_layout()
    return fig


def basefreqplotter(global_position, first_msa, freq_list, base_cov, variants_db, variants_usr, domains):
    """
    Plot the base frequency plot. Reads df, no changes

    Args:
        global_position: df of global_position table
        first_msa:
        freq_list:
        base_cov:
        variants_db: ar-like, selected variants from the db
        variants_usr: str, codons of interest provided by the user (e.g. 215;812-814)
        domains: ar-like, selected regions of interested

    Returns:
        fig

    """
    # get user data (string) into list
    # handle single codons and codon spans
    # with the format 215;812-814
    # -> ('215 (user)', [643, 645], 'variant')
    # -> ('812-814 (user)', [2434, 2440], 'variant')

    if variants_usr is not None:
        variants_user = filter_for_variants_usr(variants_usr, global_position)
    else:
        variants_user = []

    # deal with variants from the db
    # with the format A475V -> ('A475V', [1495, 1497], 'variant') for later annotation in the plot
    # aa2na_gene

    if variants_db is not None:
        variants_db = filter_variants_db(variants_db, first_msa)
    else:
        variants_db = []

    domain_locs = [get_domain_span(global_position, domain_i) for domain_i in domains]
    domains = [(domain_i, [domain_loc.loc[0, "msa"], domain_loc.loc[1, "msa"]], "domain") for
               domain_i, domain_loc in zip(domains, domain_locs)]

    # create voc dict for plot labels
    spike_length = len(global_position)
    voc_ar = variants_user + variants_db
    voc_lookup, voc_lookup_placeholders = create_voc_lookup(voc_ar, spike_length)

    # make iterable for later
    annotation_ar = voc_ar + domains

    # number of first position in sequence
    start_pos = 1
    pos_dict = global_position.to_dict()

    # this will calculate the optimal length of the matrix

    row_len = math.ceil(math.sqrt(len(freq_list)))
    no_rows = math.floor(len(freq_list) / row_len) + 1
    nested_array = create_nested_array(freq_list, row_len)
    len_nested_array = len(nested_array)
    # This calculates the color scale
    tab20c_r = create_color_scale()

    y_axis_labels = [(i * row_len) + start_pos for i in range(no_rows)]
    len_y_axis_labels = len(y_axis_labels)

    position_label = create_pos_label(spike_length, start_pos, len_nested_array, row_len, pos_dict["first_case"],
                                      base_cov, voc_lookup, voc_lookup_placeholders)
    fig = create_fig(nested_array, tab20c_r, position_label, len_y_axis_labels, y_axis_labels, row_len, annotation_ar,
                     no_rows)
    return fig
