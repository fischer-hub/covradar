**Privacy Policy**

**1. Introductory Remarks**

This page is used to inform website visitors regarding our policies with the collection, use, and disclosure of personal data if anyone decided to use our Service, the CovRadar website (www.covradar.net and www.covradar.de). Personal data is information that makes it possible to identify a person. In addition to direct personal information, such as name, date of birth or telephone number or IP address, this also includes data on personal characteristics, beliefs or relationships that allow a conclusion to be drawn about a specific person. The operators of these pages take the protection of your personal data very seriously. We treat your personal data confidentially and according to the legal data protection regulations as well as this Privacy Policy. If you choose to use our Service, then you agree to the collection and use of information in relation with this policy. The personal information that we collect is used for providing and improving the Service. We will not use or share your information with anyone except as described in this Privacy Policy. We would like to point out that the data transmission on the Internet (e.g. communication by e-mail) can have security gaps. A complete protection of data against access by third parties is not possible.

**2. General Information on Data Processing**

As a matter of principle, we collect and use personal data of our users only to the extent necessary for providing a functional website and service and as the legal requirements allow.

**3. Contact Details**

Hasso-Plattner-Institut

FG Bernhard Renard

Prof.-Dr.-Helmert-Straße 2 – 3

14482 Potsdam

**4. Data Protection officer**

Prof. Dr. Bernhard Renard

**5. Purpose**

The purpose of the CovRadar service is to provide insights into mutation frequency in public SARS-COV-2 (SC2) genomes as well as temporal and spatial distributions of genomic SC2 variants.

**6. Scope of Data Processing**

**Storage of Browser Data**

Whenever you visit our website, our system automatically collects data and information from the computer system of the calling computer. The following data is collected:

- Information about the browser type and the version used
- The user&#39;s operating system
- The Internet service provider of the user
- The IP address of the user
- Date and time of access
- Websites from which the user&#39;s system accesses our website
- Visited domain
- Timestamp of the visit
- Status code
- Size of the response body
- Referrer sent by the client
- User agent sent by the client

The data is also stored in the log files of our system, but not linked with any other personal data of the user. The temporary storage of the IP address by the system is necessary to enable the website to be delivered to the user&#39;s computer. For this purpose, the user&#39;s IP address must remain stored for the duration of the session. The storage in log files is done to ensure the functionality of the website. In addition, the data is used to optimize the website and to ensure the security of our information technology systems. An evaluation of the data for marketing purposes does not take place in this context. The data is deleted when the respective session is ended. If the data is stored in log files, it will be deleted after two weeks at the latest.

**Use of Cookies**

This website does not use cookies to collect personally identifiable information about you as a user of the site.

**7. External Links**

Our Service may contain links to other sites. If you click on a third-party link, you will be directed to that site. Note that these external sites are not operated by us. Therefore, we strongly advise you to review the Privacy Policy of these websites. We have no control over, and assume no responsibility for the content, privacy policies, or practices of any third-party sites or services.

**8. Children&#39;s Privacy**

Our Services do not address anyone under the age of 18. We do not knowingly collect personal data from children under 13. In the case we discover that a child under 18 has provided us with personal data, we immediately delete this from our servers. If you are a parent or guardian and you are aware that your child has provided us with personal information, please contact us so that we will be able to do necessary actions.

**9. Your Rights**

As a person concerned, you may at any time exercise the rights granted to you by the EU-DSGVO, insofar as they apply to the processing:

- the right to be informed whether and which of your data are being processed (Art. 15 DSGVO)
- the right to request the correction or completion of data concerning you (Art. 16 DSGVO)
- the right to have data relating to you deleted in accordance with Art. 17 DSGVO
- the right to request, in accordance with Art. 18 DPA, a restriction on the processing of data
- the right to object to future processing of data concerning you in accordance with Art. 21 DPA

The collection of data for the provision of the web offer and the storage of the data in log files is mandatory for the operation of the web pages. There is therefore no possibility of objection on the part of the user. Although IP addresses are regarded as personal data, they are not assigned to any person, so they cannot be assigned to any user and therefore cannot be accessed when information is requested. In addition to the aforementioned rights, you have the right to lodge a complaint with the data protection supervisory authority (Art. 77 DSGVO).

**10. Validity of this Privacy Policy**

Unless otherwise specified in this privacy statement, the regulations of the de.NBI Privacy Policy (https://www.denbi.de/privacy-policy) apply. We may update our Privacy Policy from time to time. Thus, we advise you to review this page periodically for any changes. We will notify you of any changes by posting the new Privacy Policy on this page. These changes are effective immediately, after they are posted on this page. If you have any questions or suggestions about our Privacy Policy, do not hesitate to contact us.