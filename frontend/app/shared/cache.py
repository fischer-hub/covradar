from flask_caching import Cache
from sqlalchemy.engine.url import make_url
import os

# This implements a caching solution using redis. It is optional and only active when a redis URL is provided via
# the environment. To prevent database overflow, use the allkeys-lru eviction policy (Attention: Do not use when you use 
# redis for other purposes, since this will delete old keys)
# Make sure the cache is wiped when the MySQL databases are updated,
# otherwise the old cached data is reused!
# How to wipe: clear redis manually (on unix, use redis-cli and then type FLUSHDB). alternatively, you could use cache.clear()

# HOW TO USE
# 1. Find slow callbacks (hint: Chromium-based browsers have a nice devtool to analyse network requests)
#     Note: Don't just apply memoize to everything, I tried and it failed horribly
# 2. Import cache from this package with
# from frontend.app.shared.cache import cache
# 3. Add the @cache.memoize() decorator AFTER @app.callback but BEFORE the function definition
#     Note: You can also cache non-callback functions, e.g. shared database fetching functions

# NOTES ON COMPONENTS USED IN GERMANY AND GLOBAL APP
# Caching uses the function name and parameter values as key. To make sure that global and global_latest versions are cached separately,
# use Input('url', 'pathname') as parameter for the Dash callback


# Redis cache is enabled if a url or host/port/db/(user/password) is provided via the environment
url = os.environ.get('REDIS_CACHE_URL')
cache_exists = url is not None or os.environ.get('REDIS_CACHE_HOST') is not None

if url:
    url = make_url(url)
    host = url.host
    port = url.port
    db = url.database
    password = url.password
    user = url.username
elif cache_exists:
    host = os.environ.get('REDIS_CACHE_HOST')
    port = os.environ.get('REDIS_CACHE_PORT')
    db = os.environ.get('REDIS_CACHE_DB')
    password = os.environ.get('REDIS_CACHE_PASSWORD')
    user = os.environ.get('REDIS_CACHE_USER')
else:
    host = None
    port = None
    db = None
    password = None
    user = None

# null cache = disable caching
cache_type = 'RedisCache' if cache_exists else 'NullCache'
cache = Cache(config={
    'CACHE_TYPE': cache_type,
    # this is fine since cache is only invalid after pipeline run
    "CACHE_DEFAULT_TIMEOUT": 60*60*24*7,
    "CACHE_REDIS_HOST": host,
    "CACHE_REDIS_PORT": port or 6379,
    "CACHE_REDIS_DB": db or 0,
    "CACHE_REDIS_PASSWORD": password,
    "CACHE_REDIS_USER": user,
    # if the source code changes, the cache is invalidated
    "CACHE_SOURCE_CHECK": True,
    "DEBUG": True if os.environ.get('DEBUG_REDIS') == 'true' else False,
})