#!/usr/bin/env python3

from Bio import SeqIO
import pandas as pd
import argparse


def extract_gene_from_multifasta(query_sequences_path, ref_gene_path, alignments_path, metadata_path, extracted_genes_path, metadata_output_path, discarded_genes_path, discarded_metadata_path, max_ns, max_length_deviation):

    counter = 0
    for record in SeqIO.parse(ref_gene_path, "fasta"):
        counter = counter + 1
        ref_gene = record
    
    if counter == 1:
        gene_length = len(ref_gene.seq)
        gene_sequence = ref_gene.seq
        alignments = pd.read_csv(alignments_path, header=None, sep='\t', skiprows=5)
        metadata = pd.read_csv(metadata_path, sep='\t')
        labels = ['Match', 'Mismatch', 'RepMatch', 'Ns', 'QGapCount', 'QGapBases',
                    'TGapCount', 'TGapBases', 'Strand', 'QName', 'QSize', 'QStart',
                    'QEnd', 'TName', 'TSize', 'TStart', 'TEnd',
                    'BlockCount', 'BlockSizes', 'QStarts', 'TStarts']
        alignments.columns = labels
        redundant_alignments = []
        for i in range(1, alignments.shape[0]):
            if alignments.at[i, 'QName'] == alignments.at[i - 1, 'QName']:
                redundant_alignments.append(i)
        alignments.drop(alignments.index[redundant_alignments], inplace=True)
        alignments.reset_index(drop=True, inplace=True)
        query_sequences = {}
        for record in SeqIO.parse(query_sequences_path, "fasta"):
            query_sequences[record.description] = record
        query_metadata = {}
        for i in range(metadata.shape[0]):
            query_metadata[metadata.loc[i, 'strain']] = metadata.iloc[i, :]
        extracted_genes = []
        discarded_genes = []
        extracted_metadata = []
        discarded_metadata = []
        for i in range(alignments.shape[0]):
            query_id = alignments.at[i, 'QName']
            query_start = min(map(int, alignments.at[i, 'QStarts'][:-1].split(','))) - min(map(int, alignments.at[i, 'TStarts'][:-1].split(',')))
            query_end = alignments.at[i, 'QEnd'] + (gene_length - alignments.at[i, 'TEnd'])
            query_sequences[query_id].seq = query_sequences[query_id].seq[query_start:query_end]
            query_ns = str(query_sequences[query_id].seq).upper().count('N')
            if query_id in query_sequences and query_id in query_metadata:
                if len(str(query_sequences[query_id].seq)) > 0:
                    ns_percentage = query_ns * 100 / len(str(query_sequences[query_id].seq))
                    query_length = len(str(query_sequences[query_id].seq))
                    length_deviation = abs(query_length - gene_length) * 100 / gene_length
                    if ns_percentage <= max_ns and length_deviation <= max_length_deviation:
                        extracted_genes.append(query_sequences[query_id])
                        extracted_metadata.append(query_metadata[query_id])
                    else:
                        query_sequences[query_id].description = 'Length: ' + str(query_length) + '; Mismatches: ' + str(alignments.at[i, 'Mismatch']) + '; Ns: ' + str(query_ns) + '; Gap length: ' + str(alignments.at[i, 'QGapBases'])
                        discarded_genes.append(query_sequences[query_id])
                        discarded_metadata.append(query_metadata[query_id])
                else:
                    print("Failed to retrieve subsequence for %s" % (query_id))
                    print("Query Start:", query_start, "/ Query End:", query_end, "/ Strand:", alignments.at[i, 'Strand'])
            else:
                print("%s not found in metadata" % (query_id))
        SeqIO.write(extracted_genes, extracted_genes_path, 'fasta')
        SeqIO.write(discarded_genes, discarded_genes_path, 'fasta')
        extracted_metadata = pd.DataFrame(extracted_metadata)
        extracted_metadata.to_csv(metadata_output_path, index=False, sep='\t')
        discarded_metadata = pd.DataFrame(discarded_metadata)
        discarded_metadata.to_csv(discarded_metadata_path, index=False, sep='\t')
    else:
        print('Error: Gene file should contain only one sequence')

if __name__ == "__main__":
    
    parser = argparse.ArgumentParser(description='Extracts genes from a multifasta, given the reference sequence and blastn alignments in tsv format')
    parser.add_argument('query_sequences', type=str, help='Input FASTA file containing the query sequences where genes need to be extracted')
    parser.add_argument('ref_gene', type=str, help='Input FASTA file containing the reference gene')
    parser.add_argument('alignments', type=str, help='Input file containing blast alignments in tsv format')
    parser.add_argument('metadata', type=str, help='Input file containing metadata in tsv format')
    parser.add_argument('extracted_genes', type=str, help='Output FASTA file containing extracted genes')
    parser.add_argument('metadata_output', type=str, help='Output file containing extracted metadata in tsv format')
    parser.add_argument('discarded_genes', type=str, help='Output FASTA file containing sequences with alignments smaller than the gene length')
    parser.add_argument('discarded_metadata', type=str, help='Output TSV file containing metadata whose sequences alignments are smaller than the gene length')
    parser.add_argument('max_ns', type=int, help='Maximum percentage of Ns allowed to keep the gene')
    parser.add_argument('max_length_deviation', type=int, help='Maximum length deviation (%) of sequence when compared with the reference gene')
    args = parser.parse_args()

    extract_gene_from_multifasta(args.query_sequences, args.ref_gene, args.alignments, args.metadata, args.extracted_genes, args.metadata_output, args.discarded_genes, args.discarded_metadata, args.max_ns, args.max_length_deviation)

