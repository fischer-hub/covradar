#!/usr/bin/env python3

from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
import pandas as pd
import argparse
import sys
sys.path.append('../')
from residue_converter.data_loader import get_reference, get_gene_pos, get_reference


def extract_ref_gene_seq(gene, out_dir, ref_path, gff_path):
    gene_dict = get_gene_pos(gff_path)
    ref_seq = get_reference(ref_path)
    ref_name = gene_dict[gene][0][0]
    gene_start = gene_dict[gene][0][1]
    gene_stop = gene_dict[gene][0][2]
    gene_seq = ref_seq[gene_start-1:gene_stop]
    id_str = ref_name+':'+str(gene_start)+'-'+str(gene_stop)+' Severe acute respiratory syndrome coronavirus 2 isolate Wuhan-Hu-1'
    gene_record = [SeqRecord(Seq(gene_seq), id=id_str, description="")]
    if out_dir[-1] == '/':
        pass
    else:
        out_dir += '/'
    out_file = out_dir+gene+'_'+ref_name+'.fasta'
    SeqIO.write(gene_record, out_file, 'fasta')

    return out_file

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Extracts gene sequence from reference sequence')
    parser.add_argument('--gene', metavar='STR', help="gene name", type=str)
    parser.add_argument('--output_dir', metavar='STR', type=str, help='output directory where the generated fasta file is saved (Default: reference_genes/)', default='reference_genes/')
    parser.add_argument('--ref_path', metavar='STR', help="FASTA file containing the reference sequence (uses SARS-CoV-2 as a reference by default, NC_045512.2)", type=str, default=None)
    parser.add_argument('--gff_path', metavar='STR', help="GFF file containing the genome annotation (uses SARS-CoV-2 as a reference by default, NC_045512.2)", type=str, default=None)
    args = parser.parse_args()

    out_file = extract_ref_gene_seq(args.gene, args.output_dir, args.ref_path, args.gff_path)