# Gene Extraction

To extract the gene sequences from a whole-genome Multi-FASTA file, the script `gene_extraction.py` can be used.
The workflow starts by creating a FASTA file for the reference gene sequence based on the input reference sequence and annotation file. Afterwards, local alignments between the reference gene and the sequences in the query Multi-FASTA file are generated using pblat. Based on the alignments the gene sequences are extracted and saved to an output Multi-Fasta file if they fulfill the requirements for the maximum percentage of nucleotides allowed to keep the gene (--max-ns) and the maximum length deviation compared to the reference gene (--max_length_deviation). The sequences that did not meet the requirements are also saved in the output folder with the prefix 'discarded_' attached to the respective files. For the required metadata file it is important to mention that it needs to contain a column named `strain` that contains the accession numbers.

## 1. Prerequisites

The scripts have some software-environmental requirements that can most easily be met by building a custom conda environment:
```
conda env create -n covradar_gene_extraction -f shared/gene_extraction/environment.yaml
```

## 2. Arguments 

| option                               | value(s)                                                                         | note                              |
|--------------------------------------|----------------------------------------------------------------------------------|-----------------------------------|
| --fasta STR                          | FASTA file containing the query sequences where the gene need to be extracted    | required                          |
| --metadata STR                       | metadata file in tsv format                                                      | required<br>need column 'strain' with accession number|
| --gene STR                           | gene name                                                                        | required                          |
| --output_dir STR                     | output directory                                                                 | Default: extracted_genes/         |
| --alignments STR                     | TSV file containing blast alignments (will be automatically generated if not given)                                                                                                                    | Default: None                     |
| --max_ns INT                         | maximum percentage of Ns allowed to keep the gene                                | Default: 5                        |
| --max_length_deviation INT           | maximum length deviation (%) of sequence when compared with the reference gene   | Default: 5                        |
| --ref_path STR                       | FASTA file containing the reference sequence                                     | Default: NC_045512.2              |
| --gff_path STR                       | GFF file containing the genome annotation                                        | Default: NC_045512.2              |
| --threads INT                        | number of threads to use                                                         | Default: all available                        |

The `--gene` argument can be set to one of the following options:
ORF1ab, S, ORF3a, E, M, ORF6, ORF7a, ORF7b, ORF8, N or ORF10.

## Exemplary Execution

Note: The scrip need to be executed inside the folder `shared/gene_extraction/` to properly work.

```
# extract spike genes from sequences
python gene_extraction.py --fasta test_genomes.fasta --metadata test_metadata.tsv --gene S
```
