import argparse
from nt_aa_translation import translate_aa_to_nt as rc_translate_aa_to_nt
from nt_aa_translation import translate_nt_to_aa as rc_translate_nt_to_aa
from nt_aa_translation import translate_nt_seq_aa_seq as rc_translate_nt_seq_aa_seq
from position_converter import aa2na_gene as pc_aa2na_gene
from position_converter import aa2na_genome as pc_aa2na_genome
from position_converter import na2aa_gene as pc_na2aa_gene
from position_converter import na2aa_genome as pc_na2aa_genome
from position_converter import na_gene_to_genome as pc_na_gene_to_genome
from position_converter import na_genome_to_gene as pc_na_genome_to_gene


def parse_args():
    parser = argparse.ArgumentParser(description="")
    subparsers = parser.add_subparsers(help='Translate amino acid mutations to nt mutations and vice versa')
    subparsers.dest = 'tool'
    subparsers.required = True
    general_parser = argparse.ArgumentParser(add_help=False)

    # create the parser for the "aa2nt" command
    parser_aa2nt = subparsers.add_parser('aa2nt', parents=[general_parser], help='Reverse-translates an AA substitution or deletion pattern into the corresponding possible nt mutation pattern(s)')
    parser_aa2nt.add_argument('--mut', metavar='STR', help="amino acid mutation pattern (e.g N501Y, HV69-70del)", type=str, required=True)
    parser_aa2nt.add_argument('--gene', metavar='STR', help="gene name (Default: S)", type=str, default="S")
    parser_aa2nt.add_argument('--nt_possible_mutations_distance', metavar='STR', help="maximum Hamming distance between the reference codon and possible mutated codons (Default: 1)", type=str, default=1)
    parser_aa2nt.add_argument('--ref_path', metavar='STR', help="FASTA file containing the reference sequence (uses SARS-CoV-2 as a reference by default, NC_045512.2)", type=str, default=None)
    parser_aa2nt.add_argument('--gff_path', metavar='STR', help="GFF file containing the genome annotation (uses SARS-CoV-2 as a reference by default, NC_045512.2)", type=str, default=None)

    # create the parser for the "nt2aa" command
    parser_nt2aa = subparsers.add_parser('nt2aa', parents=[general_parser], help='Translates an nt substitution pattern (genomic position) into the corresponding AA substitution pattern')
    parser_nt2aa.add_argument('--mut', metavar='STR', help="nucleotide mutation pattern (e.g G22132T, del:21767:6). Warning: only nt deletions starting at the first position of the codon"
                                                            " are currently supported. Multiple nt deletions may correspond to one AA deletion; only the one deleting whole codons will be reported.",
                                                             type=str, required=True)
    parser_nt2aa.add_argument('--gene', metavar='STR', help="gene name (Default: S)", type=str, default="S")
    parser_nt2aa.add_argument('--nt_possible_mutations_distance', metavar='STR', help="maximum Hamming distance between the reference codon and possible mutated codons (Default: 1)", type=str, default=1)
    parser_nt2aa.add_argument('--ref_path', metavar='STR', help="FASTA file containing the reference sequence (uses SARS-CoV-2 as a reference by default, NC_045512.2)", type=str, default=None)
    parser_nt2aa.add_argument('--gff_path', metavar='STR', help="GFF file containing the genome annotation (uses SARS-CoV-2 as a reference by default, NC_045512.2)", type=str, default=None)

    # create the parser for the "nt_seq2aa_seq" command
    parser_nt_seq2aa_seq = subparsers.add_parser('nt_seq2aa_seq', parents=[general_parser], help='Translates nucleotide sequence to corresponding amino acid sequence')
    parser_nt_seq2aa_seq.add_argument('--seq', '--nt_sequence', metavar='STR', help="sequence of nucleotides divisible by 3 (e.g. GGTACT)", type=str, required=True)

    # create the parser for the "aa2na_gene" command
    parser_aa2na_gene = subparsers.add_parser('aa2na_gene', parents=[general_parser], help='Convert amino acid position to gene position')
    parser_aa2na_gene.add_argument('--aa_pos', metavar='INT', help="amino acid position", type=int, required=True)

    # create the parser for the "aa2na_genome" command
    parser_aa2na_genome = subparsers.add_parser('aa2na_genome', parents=[general_parser], help='Convert amino acid position within gene to whole-genome position')
    parser_aa2na_genome.add_argument('--aa_pos', metavar='INT', help="amino acid position within gene", type=int, required=True)
    parser_aa2na_genome.add_argument('--gene', metavar='STR', help="gene name (Default: S)", type=str, default="S")

    # create the parser for the "na2aa_gene" command
    parser_na2aa_gene = subparsers.add_parser('na2aa_gene', parents=[general_parser], help='Convert nucleotide position within gene to amino acid position within gene')
    parser_na2aa_gene.add_argument('--na_pos', metavar='INT', help="gene nucleotide position", type=int, required=True)

    # create the parser for the "na2aa_genome" command
    parser_na2aa_genome = subparsers.add_parser('na2aa_genome', parents=[general_parser], help='Convert whole-genome nucleotide position to amino acid position within gene')
    parser_na2aa_genome.add_argument('--na_pos', metavar='INT', help="whole genome position", type=int, required=True)
    parser_na2aa_genome.add_argument('--gene', metavar='STR', help="gene name (Default: S)", type=str, default="S")

    # create the parser for the "na_gene_to_genome" command
    parser_na_gene_to_genome = subparsers.add_parser('na_gene_to_genome', parents=[general_parser], help='Convert gene position to whole-genome position')
    parser_na_gene_to_genome.add_argument('--na_pos', metavar='INT', help="gene nucleotide position", type=int, required=True)
    parser_na_gene_to_genome.add_argument('--gene', metavar='STR', help="gene name (Default: S)", type=str, default="S")

    # create the parser for the "na_genome_to_gene" command
    parser_na_genome_to_gene = subparsers.add_parser('na_genome_to_gene', parents=[general_parser], help='Convert whole-genome position to gene position')
    parser_na_genome_to_gene.add_argument('--na_pos', metavar='INT', help="whole-genome position", type=int, required=True)
    parser_na_genome_to_gene.add_argument('--gene', metavar='STR', help="gene name (Default: S)", type=str, default="S")

    return parser.parse_args()


class Translation():

    def translate_aa_to_nt(self, aa_mutation_pattern, gene, nt_possible_mutations_distance, ref_path, gff_path):
        print(rc_translate_aa_to_nt(aa_mutation_pattern, gene, nt_possible_mutations_distance, ref_path, gff_path))

    def translate_nt_to_aa(self, nt_mutation_pattern, gene, ref_path, gff_path):
        print(rc_translate_nt_to_aa(nt_mutation_pattern, gene, ref_path, gff_path))

    def translate_nt_seq_aa_seq(self, nt_sequence):
        print(rc_translate_nt_seq_aa_seq(nt_sequence))


class PositionConverter():

    def aa2na_gene(self, aa):
        print(pc_aa2na_gene(aa))

    def aa2na_genome(self, aa, gene):
        print(pc_aa2na_genome(aa, gene))

    def na2aa_gene(self, na):
        print(pc_na2aa_gene(na))

    def na2aa_genome(self, na, gene):
        print(pc_na2aa_genome(na, gene))

    def na_gene_to_genome(self, na, gene):
        print(pc_na_gene_to_genome(na, gene))

    def na_genome_to_gene(self, na, gene):
        print(pc_na_genome_to_gene(na, gene))


if __name__ == "__main__":
    args = parse_args()
    trans = Translation()
    poscon = PositionConverter()
    
    if args.tool == "aa2nt":
        trans.translate_aa_to_nt(args.mut, args.gene, args.nt_possible_mutations_distance, args.ref_path, args.gff_path)

    if args.tool == "nt2aa":
        trans.translate_nt_to_aa(args.mut, args.gene, args.ref_path, args.gff_path)
    
    if args.tool == "nt_seq2aa_seq":
        trans.translate_nt_seq_aa_seq(args.seq)

    if args.tool == "aa2na_gene":
        poscon.aa2na_gene(args.aa_pos)

    if args.tool == "aa2na_genome":
        poscon.aa2na_genome(args.aa_pos, args.gene)

    if args.tool == "na2aa_gene":
        poscon.na2aa_gene(args.na_pos)

    if args.tool == "na2aa_genome":
        poscon.na2aa_genome(args.na_pos, args.gene)

    if args.tool == "na_gene_to_genome":
        poscon.na_gene_to_genome(args.na_pos, args.gene)

    if args.tool == "na_genome_to_gene":
        poscon.na_genome_to_gene(args.na_pos, args.gene)